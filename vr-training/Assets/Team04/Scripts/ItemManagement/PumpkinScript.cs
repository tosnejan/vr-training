﻿//============
// Edit: Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

//using UnityEngine.XR.Interaction.Toolkit;

/// <summary>
/// Grows pumpkin.
/// </summary>
public class PumpkinScript : Vegetable
{

    [SerializeField]
    private int numberOfWater;
    private GameObject clayPlace;
    private GameObject progressObject;
    private QuestManagerT4 _questManagerT4;
    public GameObject pumpkin;

    [HideInInspector]
    public int waterCounter;
    public float growingTime = 20.0f;
    private float originGrowingTime;
    private float scaleCount = 10f;
    private ChopDetector hoeChopDetector;
    private bool collideWithHoe = false;
    private AudioSource audio;
    private Quaternion defaultRotation;
    private Collider collider; 
    private IgnoreHovering ignoreHovering; 

    // Start is called before the first frame update
    void Start()
    {
        if((int)state < 1)
            state = VegetableState.None;
        progressObject = GameObject.FindGameObjectWithTag("Progress");
        hoeChopDetector = GameObject.FindGameObjectWithTag("Hoe").GetComponentInParent<ChopDetector>();
        _questManagerT4 = GameObject.FindGameObjectWithTag("QuestManager").GetComponent<QuestManagerT4>();
        collider = GetComponentInChildren<Collider>();
        audio = gameObject.GetComponent<AudioSource>();
        progressObject.SetActive(false);
        defaultRotation = transform.rotation;

        originGrowingTime = growingTime;
        type = "Pumpkin";

        if (hoeChopDetector){
            hoeChopDetector.chopEvent.AddListener(HandleChopEvent);
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (this.gameObject != null)
        {
            if (InProgress)
            {
                growingTime -= Time.deltaTime;

                if (growingTime <= 0)
                {
                    InProgress = false;
                    growingTime = originGrowingTime;

                    if (state == VegetableState.Watered)
                    {
                        IncreaseVegetableScale();
                    }
                    else if (state == VegetableState.Dug)
                    {
                        IncreaseVegetableScale();
                        AddInterfactAndRigit();
                        RemoveVegetableFromPlace();
                        transform.position += new Vector3(0, 0.5f, 0);
                        state = VegetableState.Harvested;
                    }
                    else if (state == VegetableState.Planted)
                    {
                        IncreaseVegetableScale();
                    }

                    progressObject.SetActive(false);
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Hoe")&& state == VegetableState.Watered && !InProgress)
        {
            collideWithHoe = true;
        }
        else if (other.CompareTag("ClayPlace") && state == VegetableState.None)
        {
            bool isEmpty = !(other.gameObject.transform.GetChild(0).GetComponent<ArrowScript>().hasVegetable);

            if (isEmpty)
            {
                clayPlace = other.gameObject;
                state = VegetableState.Planted;

                RemoveInteractAndRigit();
                CenterVegetable(other.gameObject);

                InProgress = true;
                progressObject.SetActive(true);

                AddVegetableToPlace();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        
        if (other.CompareTag("Hoe"))        {
            collideWithHoe = false;
        }
    }

    void IncreaseVegetableScale()
    {
        pumpkin.transform.localScale += new Vector3(scaleCount, scaleCount, scaleCount);
    }
    /// <summary>
    /// Turns off interactions and physics.
    /// </summary>
    public void RemoveInteractAndRigit()
    {
        //Destroy(transform.GetComponent<XRGrabInteractable>());
        //Destroy(transform.GetComponent<Rigidbody>());
        if (gameObject.GetComponent<Interactable>().attachedToHand){
            gameObject.GetComponent<Interactable>().attachedToHand.DetachObject(gameObject);
        }
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        ignoreHovering = collider.gameObject.AddComponent<IgnoreHovering>();
    }
    /// <summary>
    /// Turns on interactions and physics.
    /// </summary>
    void AddInterfactAndRigit()
    {
        //gameObject.AddComponent<XRGrabInteractable>();
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
        Destroy(ignoreHovering);
    }

    void CenterVegetable(GameObject gameObject)
    {
        transform.rotation = defaultRotation;
        transform.position = new Vector3(gameObject.transform.position.x, -0.1f, gameObject.transform.position.z);
    }
    /// <summary>
    /// Adds water and check for progress.
    /// </summary>
    override public void AddWaterCounter()
    {
        if (!InProgress) {
            waterCounter++;

            if (state == VegetableState.Planted && waterCounter >= numberOfWater)
            {
                InProgress = true;
                state = VegetableState.Watered;
                collider.isTrigger = true;
                progressObject.SetActive(true);
            }
        }
    }
    /// <summary>
    /// Safely destroys game object.
    /// </summary>
    override public void DestroyObject()
    {
        if (gameObject.GetComponent<Interactable>().attachedToHand){
            gameObject.GetComponent<Interactable>().attachedToHand.DetachObject(gameObject);
        }
        Destroy(gameObject);
    }


    void OnAttachedToHand(Hand hand){
        if(state == VegetableState.None)
            _questManagerT4.ShowArrows(hand.handType == SteamVR_Input_Sources.LeftHand);
    }
    void OnDetachedFromHand(Hand hand){
        _questManagerT4.HideArrows(hand.handType == SteamVR_Input_Sources.LeftHand);
        GetComponent<Rigidbody>().isKinematic = false;
    }
    /// <summary>
    /// Adds information about it self to arrow.
    /// </summary>
    public void AddVegetableToPlace()
    {
        if (clayPlace == null) return;
        foreach (Transform child in clayPlace.transform)
        {
            if (child.tag == "Arrow")
            {
                child.gameObject.GetComponent<ArrowScript>().hasVegetable = true;
            }
        }
    }

    /// <summary>
    /// Removes information about it self from arrow.
    /// </summary>
    public void RemoveVegetableFromPlace()
    {
        if (clayPlace == null) return;
        foreach (Transform child in clayPlace.transform)
        {
            if (child.tag == "Arrow")
            {
                child.gameObject.GetComponent<ArrowScript>().hasVegetable = false;
            }
        }
    }

    void HandleChopEvent(float time)
    {
        if (collideWithHoe)
        {
            audio.Play(0);

            state = VegetableState.Dug;
            collider.isTrigger = false;

            InProgress = true;
            progressObject.SetActive(true);
        }
    }

}
