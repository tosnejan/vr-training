﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Handles pumpkin and carrot quests and destroys delivered items.
/// </summary>
public class DeliveryPoint : MonoBehaviour
{
    private QuestManagerT4 _questManagerT4;
    public Quest CarrotQuest;
    public Quest PumpkinQuest;
    private void Start()
    {
        _questManagerT4 = GameObject.FindGameObjectWithTag("QuestManager").GetComponent<QuestManagerT4>();
        CarrotQuest.Activate();
        PumpkinQuest.Activate();
    }
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Vegetable")
        {
            Vegetable vegetableComponent = collision.gameObject.GetComponentInParent<Vegetable>();

            if (vegetableComponent != null)
            {
                if (vegetableComponent.state == Vegetable.VegetableState.Harvested)
                {
                    if (vegetableComponent.type == "Carrot")
                    {
                        //_questManagerT4.UpdateQuests(QuestT4.QuestType.Carrot);
                        CarrotQuest.Complete();
                    }
                    else if (vegetableComponent.type == "Pumpkin")
                    {
                        //_questManagerT4.UpdateQuests(QuestT4.QuestType.Pumpkin);
                        PumpkinQuest.Complete();
                    }

                    
                    /*if (collision.gameObject.GetComponentInParent<Interactable>().attachedToHand){
                        collision.gameObject.GetComponentInParent<Interactable>().attachedToHand.DetachObject(gameObject);
                    }*/
                    vegetableComponent.DestroyObject();
                }
            }
        }
    }
}
