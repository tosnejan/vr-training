﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Spawns logs at start.
/// </summary>
public class WoodSpawner : MonoBehaviour
{

    public int numberOfWood = 10;
    public GameObject prefabToSpawn;
    // Start is called before the first frame update
    void Start()
    {
        SpawnWood(numberOfWood);
    }

    /// <summary>
    /// Spawns logs.
    /// </summary>
    public void SpawnWood(int count)
    {
        for (var i = 0; i < count; i++)
        {
            GameObject wood = Instantiate(prefabToSpawn,
                new Vector3(transform.position.x, transform.position.y + (i/5)*1, transform.position.z + (i % 5) * 0.2f),
                Quaternion.identity, null);
            wood.transform.TransformPoint(Vector3.zero);
        }
    }
}
