﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Holds information if it has vegetable and is used for arrow showing.
/// </summary>
public class ArrowScript : MonoBehaviour
{

    [HideInInspector]
    public bool hasVegetable = false;
}
