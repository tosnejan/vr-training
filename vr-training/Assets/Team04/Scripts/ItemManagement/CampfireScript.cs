﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Burns wood log if thrown into fire.
/// </summary>
public class CampfireScript : MonoBehaviour
{
    public GameObject fire;

    private QuestManagerT4 _questManagerT4;
    private float fireScale = 0.5f;
    private bool questActive = false;

    void Start()
    {
        _questManagerT4 = GameObject.FindGameObjectWithTag("QuestManager").GetComponent<QuestManagerT4>();
    }

    void Update()
    {
        if(fireScale > 0.05)
        {
            fireScale -= Time.deltaTime * 0.002f;
        }

        questActive = _questManagerT4.Quests.Exists(q => q.type == QuestT4.QuestType.Fire);

        if(fireScale < 0.2 && !questActive)
        {
            _questManagerT4.SpawnWood(3);
            _questManagerT4.Quests.Add(new QuestT4(QuestT4.QuestType.Fire, 3));
        }

        fire.transform.localScale = Vector3.one * fireScale;
    }

    void OnTriggerEnter(Collider collision)
    {
        string tag = collision.gameObject.tag;

        if (tag != null)
        {
            if (tag == "Log")
            {
                _questManagerT4.UpdateQuests(QuestT4.QuestType.Fire);
                fireScale += 0.2f;
                fireScale = Mathf.Clamp(fireScale, 0.05f, 1);
                collision.gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (tag != null)
        {
            if (tag == "Log")
            {
                Destroy(collision.gameObject);
            }
        }
    }

}
