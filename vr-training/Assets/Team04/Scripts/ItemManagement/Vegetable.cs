﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Vegetable interface.
/// </summary>
abstract public class Vegetable : MonoBehaviour
{

    public enum VegetableState
    {
        None,
        Planted,
        Watered,
        Dug,
        Harvested
    }

    public bool InProgress = false;
    public VegetableState state;
    public string type;
    abstract public void DestroyObject();
    abstract public void AddWaterCounter();

}
