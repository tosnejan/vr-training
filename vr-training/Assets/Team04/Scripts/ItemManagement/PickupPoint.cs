﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Spawns vegetable at start.
/// </summary>
public class PickupPoint : MonoBehaviour
{

    public GameObject prefabToSpawn;
    public int numberOfObjects;

    private bool _loadGame;

    private void Awake()
    {
        _loadGame = SaveLoadController.loadGame;
    }

    // Start is called before the first frame update
    void Start()
    {
        if(!_loadGame)
            Spawn(numberOfObjects);
    }
    /// <summary>
    /// Spawns vegetable.
    /// </summary>
    public void Spawn(int count)
    {
        for (var i = 0; i < count; i++)
        {
            GameObject carrot = Instantiate(prefabToSpawn, new Vector3(0, 0, 0), Quaternion.identity);
            carrot.transform.SetParent(gameObject.transform, false);
            carrot.transform.SetParent(null, true);
            carrot.transform.TransformPoint(Vector3.zero);
            carrot.transform.Rotate(270, 0, 0);
        }
    }
}
