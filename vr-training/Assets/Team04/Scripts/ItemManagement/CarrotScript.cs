﻿//============
// Edit: Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

/// <summary>
/// Grows carrot.
/// </summary>
public class CarrotScript : Vegetable
{

    [SerializeField]
    private int numberOfWater;
    private GameObject clayPlace;
    private GameObject progressObject;
    private QuestManagerT4 _questManagerT4;
    public GameObject carrot;

    [HideInInspector]
    public int waterCounter;
    public float growingTime = 15.0f;
    private float originGrowingTime;
    private float scaleCount = 2.5f;
    private ChopDetector hoeChopDetector;
    private bool collideWithHoe = false;
    private AudioSource audio;
    private Quaternion defaultRotation;
    private Collider collider;
    private IgnoreHovering ignoreHovering; 

    // Start is called before the first frame update
    void Start()
    {
        if ((int)state < 1)
            state = VegetableState.None;
        progressObject = GameObject.FindGameObjectWithTag("Progress");
        hoeChopDetector = GameObject.FindGameObjectWithTag("Hoe").GetComponentInParent<ChopDetector>();
        _questManagerT4 = GameObject.FindGameObjectWithTag("QuestManager").GetComponent<QuestManagerT4>();
        collider = GetComponentInChildren<Collider>();
        audio = gameObject.GetComponent<AudioSource>();
        progressObject.SetActive(false);
        defaultRotation = transform.rotation;

        originGrowingTime = growingTime;
        type = "Carrot";

        if (hoeChopDetector){
            hoeChopDetector.chopEvent.AddListener(HandleChopEvent);
            
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (this.gameObject != null)
        {
            if (InProgress)
            {
                growingTime -= Time.deltaTime;

                if (growingTime <= 0)
                {
                    InProgress = false;
                    growingTime = originGrowingTime;

                    if (state == VegetableState.Watered)
                    {
                        IncreaseVegetableScale();
                    }
                    else if (state == VegetableState.Dug)
                    {
                        IncreaseVegetableScale();
                        AddInteractAndRigit();
                        RemoveVegetableFromPlace();
                        transform.position += new Vector3(0, 0.5f, 0);
                        state = VegetableState.Harvested;
                    }
                    else if (state == VegetableState.Planted)
                    {
                        IncreaseVegetableScale();
                    }

                    progressObject.SetActive(false);
                }
            }
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Hoe" && state == VegetableState.Watered && !InProgress)
        {
            collideWithHoe = true;
        }
        else if (collision.gameObject.tag == "ClayPlace" && state == VegetableState.None)
        {
            bool isEmpty = !(collision.gameObject.transform.GetChild(0).GetComponent<ArrowScript>().hasVegetable);

            if (isEmpty) {
                clayPlace = collision.gameObject;
                state = VegetableState.Planted;

                RemoveInteractAndRigit();
                CenterVegetable(collision.gameObject);

                InProgress = true;
                progressObject.SetActive(true);

                AddVegetableToPlace();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        collideWithHoe = false;
    }

    void IncreaseVegetableScale()
    {
        carrot.transform.localScale += new Vector3(scaleCount, scaleCount, scaleCount);
    }

    /// <summary>
    /// Turns off interactions and physics.
    /// </summary>
    public void RemoveInteractAndRigit()
    {
        //Destroy(transform.GetComponent<XRGrabInteractable>());
        //gameObject.GetComponent<Collider>().isTrigger = true;
        if (gameObject.GetComponent<Interactable>().attachedToHand){
            gameObject.GetComponent<Interactable>().attachedToHand.DetachObject(gameObject);
        }
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        collider.isTrigger = true;
        ignoreHovering = collider.gameObject.AddComponent<IgnoreHovering>();
    }


    /// <summary>
    /// Turns on interactions and physics.
    /// </summary>
    void AddInteractAndRigit()
    {
        //gameObject.AddComponent<XRGrabInteractable>();
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
        collider.isTrigger = false;
        Destroy(ignoreHovering);
    }

    void CenterVegetable(GameObject gameObject)
    {
        transform.rotation = defaultRotation;
        transform.position = new Vector3(gameObject.transform.position.x, 0.1f, gameObject.transform.position.z);
    }

    /// <summary>
    /// Adds water and check for progress.
    /// </summary>
    override public void AddWaterCounter()
    {
        if (!InProgress) {
            waterCounter++;

            if (state == VegetableState.Planted && waterCounter >= numberOfWater)
            {
                InProgress = true;
                state = VegetableState.Watered;
                progressObject.SetActive(true);
            }
        }
    }

    /// <summary>
    /// Safely destroys game object.
    /// </summary>
    override public void DestroyObject()
    {
        if (gameObject.GetComponent<Interactable>().attachedToHand){
            gameObject.GetComponent<Interactable>().attachedToHand.DetachObject(gameObject);
        }
        //gameObject.SetActive(false);
        Destroy(gameObject);
    }

    void OnAttachedToHand(Hand hand){
        if(state == VegetableState.None)
            _questManagerT4.ShowArrows(hand.handType == SteamVR_Input_Sources.LeftHand);
    }
    void OnDetachedFromHand(Hand hand){
        _questManagerT4.HideArrows(hand.handType == SteamVR_Input_Sources.LeftHand);
        GetComponent<Rigidbody>().isKinematic = false;
    }

    /// <summary>
    /// Adds information about it self to arrow.
    /// </summary>
    public void AddVegetableToPlace()
    {
        if (clayPlace == null) return;
        foreach (Transform child in clayPlace.transform)
        {
            if (child.tag == "Arrow")
            {
                child.gameObject.GetComponent<ArrowScript>().hasVegetable = true;
            }
        }
    }
    /// <summary>
    /// Removes information about it self from arrow.
    /// </summary>
    public void RemoveVegetableFromPlace()
    {
        if (clayPlace == null) return;
        foreach (Transform child in clayPlace.transform)
        {
            if (child.tag == "Arrow")
            {
                child.gameObject.GetComponent<ArrowScript>().hasVegetable = false;
            }
        }
    }

    void HandleChopEvent(float time)
    {
        if (collideWithHoe)
        {
            audio.Play(0);

            state = VegetableState.Dug;

            InProgress = true;
            progressObject.SetActive(true);
        }
    }

}
