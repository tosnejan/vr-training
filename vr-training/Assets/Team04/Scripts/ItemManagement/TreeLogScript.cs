﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

//using UnityEngine.XR.Interaction.Toolkit;
/// <summary>
/// Log chopping.
/// </summary>
public class TreeLogScript : MonoBehaviour
{

    private GameObject stumpPlace;
    [HideInInspector]
    public bool chopped = false;

    private ChopDetector axeChopDetector;
    private bool axeCollision = false;
    public GameObject part1;
    public GameObject part2;
    private AudioSource audio;

    private void Start()
    {
        axeChopDetector = GameObject.FindGameObjectWithTag("Axe").GetComponent<ChopDetector>();
        audio = gameObject.GetComponent<AudioSource>();
        axeChopDetector.chopEvent.AddListener(HandleChopEvent);
    }

    void OnTriggerEnter(Collider collider)
    {
        ProceedCollision(collider.gameObject);
    }

    private void OnTriggerExit(Collider collider)
    {
        axeCollision = false;
    }

    void ProceedCollision(GameObject gameObjectCol) {

        if (gameObjectCol.tag == "Stumpplace")
        {
            stumpPlace = gameObjectCol;
            RemoveInteractAndRigit();
            CenterTreeLog(gameObjectCol);
        }
        else if (gameObjectCol.tag == "Axe" && !chopped)
        {
            axeCollision = true;
        }
    }

    void HandleChopEvent(float time)
    {
        if (axeCollision)
        {
            audio.Play(0);
            
            GameObject _part1 = Instantiate(part1, transform.position, transform.rotation);
            GameObject _part2 = Instantiate(part2, transform.position, transform.rotation);

            axeChopDetector.chopEvent.RemoveListener(HandleChopEvent);
            DestroyObject();
        }
    }

    void CenterTreeLog(GameObject gameObject)
    {
        transform.position = new Vector3(gameObject.transform.position.x, 0.88f, gameObject.transform.position.z);
        transform.eulerAngles = new Vector3(0, 0, 0);
    }

    void RemoveInteractAndRigit()
    {
        gameObject.GetComponent<Collider>().isTrigger = true;
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        if (gameObject.GetComponent<Interactable>().attachedToHand){
            gameObject.GetComponent<Interactable>().attachedToHand.DetachObject(gameObject);
        }
        gameObject.AddComponent<IgnoreHovering>();
    }
    
    /// <summary>
    /// Safely destroys game object.
    /// </summary>
    public void DestroyObject() {
        if (gameObject.GetComponent<Interactable>().attachedToHand){
            gameObject.GetComponent<Interactable>().attachedToHand.DetachObject(gameObject);
        }
        Destroy(gameObject);
    }

}
