﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Handles wood quest and destroys delivered items.
/// </summary>
public class DeliveryPointWood : MonoBehaviour
{

    private QuestManagerT4 _questManagerT4;
    public Quest WoodQuest;
    void Start()
    {
        _questManagerT4 = GameObject.FindGameObjectWithTag("QuestManager").GetComponent<QuestManagerT4>();
        WoodQuest.Activate();
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Log"))
        {
            //_questManagerT4.UpdateQuests(QuestT4.QuestType.Wood);
            WoodQuest.Complete();
            
            if (collision.gameObject.GetComponentInParent<Interactable>().attachedToHand){
                collision.gameObject.GetComponentInParent<Interactable>().attachedToHand.DetachObject(gameObject);
            }
            Destroy(collision.gameObject);
        }
    }
}
