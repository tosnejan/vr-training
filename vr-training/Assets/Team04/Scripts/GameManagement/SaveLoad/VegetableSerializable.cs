﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Vegetable;
/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
[System.Serializable]
public class VegetableSerializable
{
    public int state;
    public string type;
    public Vector3Serializable position;
    public Vector3Serializable scale;
    public QuaternionSerializable rotation;
    public bool inProgress;

    public VegetableSerializable(Vegetable vegetable)
    {
        inProgress = vegetable.InProgress;
        type = vegetable.type;
        state = (int)vegetable.state;
        position = Vector3Serializable.GetSerializable(vegetable.transform.position);
        rotation = QuaternionSerializable.GetSerializable(vegetable.transform.rotation);
        scale = Vector3Serializable.GetSerializable(vegetable.transform.localScale);
    }
}
