﻿//============
// Edit: Jan Tošner
//============
using UnityEngine;
/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
[System.Serializable]
public class WoodSerializable
{
    public string type;
    public Vector3Serializable position;
    public Vector3Serializable scale;
    public QuaternionSerializable rotation;

    public WoodSerializable(GameObject wood)
    {
        type = wood.tag;
        position = Vector3Serializable.GetSerializable(wood.transform.position);
        scale = Vector3Serializable.GetSerializable(wood.transform.localScale);
        rotation = QuaternionSerializable.GetSerializable(wood.transform.rotation);
    }
}
