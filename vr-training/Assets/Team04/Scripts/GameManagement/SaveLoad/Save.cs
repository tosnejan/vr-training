﻿//============
// Edit: Jan Tošner
//============
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
//Serializable save object, all properties must be serializable too
[System.Serializable]
public class Save
{
    //Player
    public Vector3Serializable playerPosition;
    public QuaternionSerializable playerRotation;

    //Items
    public List<VegetableSerializable> vegetable;
    public List<WoodSerializable> wood;

    //Quest
    public List<QuestT4> quests;
    public int[] questLevels;

    public int questsCounter;


    public Save(GameObject player, List<GameObject> vegetable, List<GameObject> wood, QuestManagerT4 questManagerT4)
    {
        playerPosition = Vector3Serializable.GetSerializable(player.transform.position);
        playerRotation = QuaternionSerializable.GetSerializable(player.transform.rotation);

        this.vegetable = new List<VegetableSerializable>();
        foreach(GameObject v in vegetable)
        {
            this.vegetable.Add(new VegetableSerializable(v.GetComponent<Vegetable>()));
        }

        this.wood = new List<WoodSerializable>();
        foreach (GameObject w in wood)
        {
            this.wood.Add(new WoodSerializable(w));
        }

        quests = questManagerT4.Quests;
        questLevels = questManagerT4.questLevels;
        questsCounter = questManagerT4.questsCounter;
    }
}
