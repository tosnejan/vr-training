﻿//============
// Edit: Jan Tošner
//============
using UnityEngine;
/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
//Used to save'n'load
[System.Serializable]
public class Vector3Serializable
{
    public float x;
    public float y;
    public float z;

    public Vector3Serializable(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static Vector3Serializable GetSerializable(Vector3 vector) {
        return new Vector3Serializable(vector.x, vector.y, vector.z);
    }

    public Vector3 GetVector3()
    {
        return new Vector3(x, y, z);
    }
}
