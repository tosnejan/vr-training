﻿//============
// Edit: Jan Tošner
//============
using UnityEngine;

/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
[System.Serializable]
public class QuaternionSerializable
{
    public float x;
    public float y;
    public float z;
    public float w;

    public QuaternionSerializable(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public static QuaternionSerializable GetSerializable(Quaternion q) {
        return new QuaternionSerializable(q.x, q.y, q.z, q.w);
    }

    public Quaternion GetQuaternion() {
        return new Quaternion(x, y, z, w);
    }
}
