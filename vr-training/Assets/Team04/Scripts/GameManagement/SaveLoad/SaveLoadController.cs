//============
// Edit: Jan Tošner
//============
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System;
/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
public class SaveLoadController : MonoBehaviour
{
    public static bool loadGame = false;
    public GameObject carrotPrefab, pumpkinPrefab, woodPrefab, log1Prefab, log2Prefab;
    private QuestManagerT4 _questManagerT4;
    private GameObject player;
    private System.Random rnd;
    private void Start()
    {
        _questManagerT4 = GameObject.FindGameObjectWithTag("QuestManager").GetComponent<QuestManagerT4>();
        player = GameObject.FindGameObjectWithTag("Player");
        rnd = new System.Random();

        if (loadGame)
        {
            LoadGame();
            loadGame = false;
        }
    }
    public void SaveGame()
    {
        Save save = CreateSave();
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Create(GetSavePath());
        binaryFormatter.Serialize(file, save);
        file.Close();

        Debug.Log("Game saved!");
    }


    Save CreateSave()
    {
        List<GameObject> vegetable = new List<GameObject> (GameObject.FindGameObjectsWithTag("Vegetable"));
        List<GameObject> wood = new List<GameObject>(GameObject.FindGameObjectsWithTag("Wood"));
        wood.AddRange(GameObject.FindGameObjectsWithTag("Log"));

        return new Save(player, vegetable, wood, _questManagerT4);
    }

    public void LoadGame()
    {
        Save save = GetSaveFile();
        if (save != null)
        {
            player.transform.position = new Vector3(save.playerPosition.x, save.playerPosition.y, save.playerPosition.z);
            player.transform.rotation = new Quaternion(save.playerRotation.x, save.playerRotation.y, save.playerRotation.z, save.playerRotation.w);

            //Vegetable
            GameObject[] vegetable = GameObject.FindGameObjectsWithTag("Vegetable");
            foreach (GameObject v in vegetable)
            {
                v.SetActive(false);
                Destroy(v);
            }

            foreach (VegetableSerializable v in save.vegetable)
            {
                SpawnVegetable(v);
            }


            //Wood
            GameObject[] wood = GameObject.FindGameObjectsWithTag("Wood");
            foreach (GameObject w in wood)
            {
                w.SetActive(false);
                Destroy(w);
            }
            GameObject[] logs = GameObject.FindGameObjectsWithTag("Log");
            foreach (GameObject l in logs)
            {
                l.SetActive(false);
                Destroy(l);
            }

            foreach (WoodSerializable w in save.wood)
            {
                SpawnWood(w);
            }

            _questManagerT4.Quests = save.quests;
            _questManagerT4.questLevels = save.questLevels;
            _questManagerT4.questsCounter = save.questsCounter;
            _questManagerT4.UpdateQuestCounter();

            Debug.Log("Game loaded!");
        }

    }

    private void SpawnVegetable(VegetableSerializable v)
    {
        switch (v.type)
        {
            case "Carrot":
                GameObject carrot = Instantiate(carrotPrefab, v.position.GetVector3(), v.rotation.GetQuaternion(), null);
                carrot.transform.localScale = v.scale.GetVector3();

                carrot.GetComponent<CarrotScript>().InProgress = v.inProgress;

                Vegetable.VegetableState s1 = (Vegetable.VegetableState)v.state;
                carrot.GetComponent<CarrotScript>().state = s1;
                if (s1 == Vegetable.VegetableState.Planted || s1 == Vegetable.VegetableState.Watered)
                    carrot.GetComponent<CarrotScript>().RemoveInteractAndRigit();
                break;
            case "Pumpkin":
                GameObject pumpkin = Instantiate(pumpkinPrefab, v.position.GetVector3(), v.rotation.GetQuaternion(), null);
                pumpkin.transform.localScale = v.scale.GetVector3();

                pumpkin.GetComponent<PumpkinScript>().InProgress = v.inProgress;

                Vegetable.VegetableState s2 = (Vegetable.VegetableState)v.state;
                pumpkin.GetComponent<PumpkinScript>().state = s2;
                if (s2 == Vegetable.VegetableState.Planted || s2 == Vegetable.VegetableState.Watered)
                    pumpkin.GetComponent<PumpkinScript>().RemoveInteractAndRigit();
                break;
            default:
                break;
        }
    }

    private void SpawnWood(WoodSerializable w)
    {
        switch (w.type)
        {
            case "Wood":
                GameObject wood = Instantiate(woodPrefab, w.position.GetVector3(), w.rotation.GetQuaternion(), null);
                wood.transform.localScale = w.scale.GetVector3();
                break;
            case "Log":
                if(rnd.Next(2) > 0)
                {
                    GameObject log = Instantiate(log1Prefab, w.position.GetVector3(), w.rotation.GetQuaternion(), null);
                    log.transform.localScale = w.scale.GetVector3();
                }
                else
                {
                    GameObject log = Instantiate(log2Prefab, w.position.GetVector3(), w.rotation.GetQuaternion(), null);
                    log.transform.localScale = w.scale.GetVector3();
                }
                break;
            default:
                break;
        }
    }

    public static Save GetSaveFile() {
        Save save = null;
        string savePath = GetSavePath();

        if (File.Exists(savePath))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream file = File.Open(savePath, FileMode.Open);
            save = (Save)binaryFormatter.Deserialize(file);
            file.Close();
        }

        return save;
    }

    public static bool AlreadySaved() {
        bool result = File.Exists(GetSavePath());
        return result;
    }

    static string GetSavePath() {
        return Application.persistentDataPath + "/save.dat";
    }

}
