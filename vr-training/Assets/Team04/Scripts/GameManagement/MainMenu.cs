﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
public class MainMenu : MonoBehaviour
{
    public Button buttton;
    private void Start()
    {
        // SceneManager.sceneLoaded += CheckSave;
        buttton.interactable = SaveLoadController.AlreadySaved();
    }
    public void GameStart()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("QUIT");
    }

    public void OnLoad()
    {
        SaveLoadController.loadGame = true;
        SceneManager.LoadScene(1);
    }

    private void CheckSave(Scene scene, LoadSceneMode mode)
    {
        if(scene.buildIndex == 0)
        {
            buttton.interactable = SaveLoadController.AlreadySaved();
        }

    }

    
}
