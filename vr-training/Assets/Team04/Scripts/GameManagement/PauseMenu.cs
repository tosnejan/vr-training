﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;
/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
public class PauseMenu : MonoBehaviour
{
    public GameObject canvas;
    public GameObject camera;
    public Button loadButton;

    private List<InputFeatureUsage> primaryButton = new List<InputFeatureUsage>() { (InputFeatureUsage)CommonUsages.primaryButton };

    private bool paused = false;

    private bool wasPressed = false;
    public SaveLoadController saveLoadController;

    private void Start()
    {
        UpdatePauseMenu();
        CheckSave();
    }

    void Update()
    {
        var rightHandedControllers = new List<InputDevice>();
        var desiredCharacteristics = InputDeviceCharacteristics.HeldInHand | InputDeviceCharacteristics.Controller;
        InputDevices.GetDevicesWithCharacteristics(desiredCharacteristics, rightHandedControllers);

        foreach (InputDevice device in rightHandedControllers)
        {
            bool secondaryButton, menuButton, primaryButton;
            device.TryGetFeatureValue(CommonUsages.secondaryButton, out secondaryButton);
            device.TryGetFeatureValue(CommonUsages.menuButton, out menuButton);
            device.TryGetFeatureValue(CommonUsages.primaryButton, out primaryButton);

            bool pressed = secondaryButton || menuButton || primaryButton;

            //Debug.Log(device.subsystem.SubsystemDescriptor.id);
            
            if (wasPressed && !pressed)
            {
                paused = !paused;
                UpdatePauseMenu();
            }
            wasPressed = pressed;
        }
    }

    public void UpdatePauseMenu()
    {
        Time.timeScale = paused ? 0 : 1;
        canvas.SetActive(paused);
    }

    public void OnResume()
    {
        paused = false;
        UpdatePauseMenu();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void OnSave()
    {
        saveLoadController.SaveGame();
        CheckSave();
    }

    public void OnLoad()
    {
        saveLoadController.LoadGame();
        OnResume();
    }

    public void CheckSave()
    {
        loadButton.interactable = SaveLoadController.AlreadySaved();
    }


}
