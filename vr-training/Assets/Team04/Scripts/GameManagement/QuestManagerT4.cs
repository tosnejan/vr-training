﻿//============
// Edit: Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static QuestT4;
/// <summary>
/// Almost not used. Using only showing arrows.
/// Left for no issues.
/// </summary>
public class QuestManagerT4 : MonoBehaviour
{
    private bool _loadGame;
    [HideInInspector]
    public List<QuestT4> Quests
    { 
        get { return _quests; }
        set
        {
            _quests = value;
        }
    }
    public GameObject woodSpawner;
    public GameObject carrotSpawner;
    public GameObject pumpkinSpawner;
    public float timeBetweenQuests = 5;
    public SaveLoadController saveLoadController;

    private WoodSpawner _woodSpawner;
    private PickupPoint _carrotSpawner;
    private PickupPoint _pumpkinSpawner;

    private Text questsTextComponent;
    public Text questsDoneTextComponent;
    [SerializeField]
    private GameObject questsSign;
    private GameObject[] arrows;

    private List<QuestT4> _quests = new List<QuestT4>();
    [HideInInspector]
    public int[] questLevels = {0,0,0,0};
    private System.Random rnd = new System.Random();
    private float timeCounter;
    private bool questsPause = false;
    public int questsCounter = 0;

    private bool left = false;
    private bool right = false;


    private void Awake()
    {
        _loadGame = SaveLoadController.loadGame;
    }
    // Start is called before the first frame update
    void Start()
    {
        questsTextComponent = questsSign.GetComponent<Text>();
        arrows = GameObject.FindGameObjectsWithTag("Arrow");
        _woodSpawner = woodSpawner.GetComponent<WoodSpawner>();
        _carrotSpawner = carrotSpawner.GetComponent<PickupPoint>();
        _pumpkinSpawner = pumpkinSpawner.GetComponent<PickupPoint>();

        HideArrows(false);

        if (!_loadGame)
            _quests.Add(GenerateQuest());

        UpdateQuestCounter();
    }

    private QuestT4 GenerateQuest()
    {
        QuestType type = (QuestType)rnd.Next(0, 3);
        int level = ++questLevels[(int)type];
        switch (type)
        {
            case QuestType.Wood:
                SpawnWood(level * level);
                break;
            case QuestType.Carrot:
                SpawnCarrots(level * level);
                break;
            case QuestType.Pumpkin:
                SpawnPumpkins(level * level);
                break;
            default:
                break;
        }
        if(type == QuestType.Wood)
        {
            SpawnWood(level * level);
        }
        return new QuestT4(type, level*level);
    }

    public void SpawnWood(int count)
    {
        _woodSpawner.SpawnWood(count);
    }

    public void SpawnCarrots(int count)
    {
        _carrotSpawner.Spawn(count);
    }
    public void SpawnPumpkins(int count)
    {
        _pumpkinSpawner.Spawn(count);
    }

    public void PlayParticle() 
    {

        ParticleSystem particle = GameObject.FindGameObjectWithTag("Fireworks").GetComponent<ParticleSystem>();
        AudioSource celebrateSound = gameObject.GetComponent<AudioSource>();

        particle.Play();
        celebrateSound.Play(0);
    }

    public void UpdateQuests(QuestType type)
    {
        List<QuestT4> qs = Quests.FindAll(q => q.type == type);
        foreach(QuestT4 q in qs)
        {
            q.progress++;
        }
        bool done = Quests.TrueForAll(q => q.finished);
        if (done)
        {
            questsCounter++;
            PlayParticle();
            _quests.Clear();

            timeCounter = timeBetweenQuests;
            questsPause = true;

            UpdateQuestCounter();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (questsPause)
        {
            timeCounter -= Time.deltaTime;
            questsTextComponent.text = "Wait a few seconds new quest";

            if (timeCounter <= 0) {
                questsPause = false;
                _quests.Add(GenerateQuest());
            }
        }
        else
        {
            string text = "";
            foreach (QuestT4 q in Quests)
            {
                text += q.ToString() + "\n";
            }

            questsTextComponent.text = text;
        }
    }

    public void ShowArrows(bool _left){
        if (_left)left = true;
        else right = true;
        
        foreach (GameObject arrow in arrows)
        {
            if (!arrow.GetComponent<ArrowScript>().hasVegetable)
            {
                arrow.SetActive(true);
            }
        }
    }

    public void HideArrows(bool _left){
        if (_left)left = false;
        else right = false;

        if (left || right) return;
        foreach (GameObject arrow in arrows)
        {
            arrow.SetActive(false);
        }
    }

    public void UpdateQuestCounter()
    {
        questsDoneTextComponent.text = "Quests done - " + questsCounter;
    }

}
