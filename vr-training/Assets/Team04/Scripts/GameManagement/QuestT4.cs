﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
[System.Serializable]
public class QuestT4
{
    public enum QuestType
    {
        Carrot,
        Pumpkin,
        Wood,
        Fire
    }

    public QuestType type;
    public int quantity;
    public int progress = 0;
    public bool finished
    {
        get
        {
            return progress >= quantity;
        }
    }

    public QuestT4( QuestType type, int quantity)
    {
        this.type = type;
        this.quantity = quantity;
    }

    public override string ToString()
    {
        switch (type)
        {
            case QuestType.Carrot:
                return finished ? "Carrot quest completed!"  : "Grow carrots: " + progress + " / " + quantity;
            case QuestType.Pumpkin:
                return finished ? "Pumpkin quest completed!" : "Grow pumpkins: " + progress + " / " + quantity;
            case QuestType.Wood:
                return finished ? "Wood quest completed!" : "Chop wood: " + progress + " / " + quantity;
            case QuestType.Fire:
                return finished ? "Fire quest completed!" : "Add choped logs to fire: " + progress + " / " + quantity;
            default:
                return "";
        }
    }

}
