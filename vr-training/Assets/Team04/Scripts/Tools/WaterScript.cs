﻿//============
// Edit: Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class WaterScript : MonoBehaviour
{

    private float destroyDelay = 1.5f;

    void Start()
    {
        Object.Destroy(gameObject, destroyDelay);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Vegetable")
        {
            collision.gameObject.GetComponentInParent<Vegetable>().AddWaterCounter();
        }
    }

    private void OnTriggerEnter(Collider other){
        if (other.CompareTag("Vegetable"))
        {
            other.gameObject.GetComponentInParent<Vegetable>().AddWaterCounter();
        }
    }
}
