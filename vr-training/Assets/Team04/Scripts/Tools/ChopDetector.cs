﻿//============
// Edit: Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;
/// <summary>
/// Used for axe and hoe. Calls <see cref="ChopEvent"/> when player chops.
/// </summary>
public class ChopDetector : MonoBehaviour
{
    private float lastVelocity = 0;
    private float acceleration;
    private bool swing = false;
    private bool chop = false;
    private float chopStart;
    private Rigidbody rb;
    public ChopEvent chopEvent;
    public static bool isRight = true;
    [Range(0.5f, 5.0f)]
    public float chopSpeedLimit = 3.0f;
    [Range(-70f, -10f)]
    public float decelerationLimit = -50f;


    public class ChopEvent : UnityEvent<float>
    {
    }

    /// <summary>
    /// Not used.
    /// </summary>
    public void DominantRight()
    {
        isRight = true;
    }

    /// <summary>
    /// Not used.
    /// </summary>
    public void DominantLeft()
    {
        isRight = false;
    }

    private void Awake()
    {
        if(chopEvent == null) chopEvent = new ChopEvent();
    }

    private void Start(){
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        float newVelocity = rb.velocity.magnitude;
        if (!swing && newVelocity > chopSpeedLimit)
        {
            chopStart = Time.time;
            swing = true;
        }

        if (swing)
        {
            acceleration = (newVelocity - lastVelocity) / Time.deltaTime;
            if (acceleration < decelerationLimit)
            {
                chop = true;
            }
        }

        if(swing && (newVelocity < 0.5 || Time.time - chopStart >= 1.5f))
        {
            if (chop)
            {
                chopEvent.Invoke(Time.time - chopStart);
            }
            chop = false;
            swing = false;
        }
        lastVelocity = newVelocity;
    }
}
