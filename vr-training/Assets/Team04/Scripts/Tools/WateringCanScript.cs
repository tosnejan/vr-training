﻿//============
// Edit: Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Random;
/// <summary>
/// Instantiates water if in right angle.
/// </summary>
public class WateringCanScript : MonoBehaviour
{

    [SerializeField]
    private GameObject waterGameObject;
    [SerializeField]
    private GameObject waterSpawnPoint;
    private float Timer = 0.05f;

    // Update is called once per frame
    void Update()
    {
        bool shouldSpawnWater = gameObject.transform.rotation.eulerAngles.x > 30 && gameObject.transform.rotation.eulerAngles.x < 150;
        Timer -= Time.deltaTime;

        if (shouldSpawnWater && Timer <= 0f) {
            SpawnWater();
            Timer = 0.05f;
        }
    }
    /// <summary>
    /// Instantiates water on random position in some Range.
    /// </summary>
    private void SpawnWater() {
        GameObject watter = Instantiate(waterGameObject);
        watter.transform.position = new Vector3(
            waterSpawnPoint.transform.position.x + UnityEngine.Random.Range(-0.1f, 0.1f),
            waterSpawnPoint.transform.position.y + UnityEngine.Random.Range(-0.1f, 0.1f),
            waterSpawnPoint.transform.position.z + UnityEngine.Random.Range(-0.1f, 0.1f));
    }

}
