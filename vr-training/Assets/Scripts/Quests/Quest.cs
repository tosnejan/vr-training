﻿//============
// Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static QuestManager;

/// <summary>
/// Scriptable object used to store quests.
/// </summary>
[CreateAssetMenu]
public class Quest : ScriptableObject {
    public string message;
    public QuestEnum type;
    public int countToBeDone = 1;
    public bool done = false;
    private int currentlyDone = 0;

    public int CurrentlyDone => currentlyDone;
    
    /// <summary>
    /// Adds this Quest to active quests.
    /// </summary>
    public void Activate() {
        if (instance.activeQuests.Contains(this)) return;
        done = false;
        instance.activeQuests.Add(this);
        Reset();
    }
    
    /// <summary>
    /// Adds one to <see cref="currentlyDone"/>.
    /// Completes this quest it <see cref="currentlyDone"/> > <see cref="countToBeDone"/>.
    /// </summary>
    public void Complete(){
        currentlyDone += currentlyDone == countToBeDone ? 0 : 1;
        if (done || currentlyDone < countToBeDone) return;
        done = true;
        instance.QuestDone(type);
    }
    
    /// <summary>
    /// If quest is not done it will subtract one from <see cref="currentlyDone"/>.
    /// </summary>
    public void UnComplete(){
        if (done) return;
        currentlyDone += currentlyDone == 0 ? 0 : -1;
    }

    /// <summary>
    /// Resets quest.
    /// </summary>
    public void Reset(){
        currentlyDone = 0;
        done = false;
    }
}
