﻿//============
// Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;
/// <summary>
/// Singleton which connects <see cref="Quest"/>s with <see cref="UserData"/>.
/// </summary>
public class QuestManager : MonoBehaviour {
    public enum QuestEnum{
        Toast, Tea, Coffee, Omelette, Bacon, Juice, Cocoa, Milk,
        Chicken, Horse, Cow, Water, Wood, Carrot, Pumpkin,
        Chairs, Pillow, Soap, Cup, Bottle
    }
    public List<Quest> activeQuests = new List<Quest>();

    public static QuestManager instance;

    public UserData User;

    private void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        } else {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
    /// <summary>
    /// Resets all <see cref="Quest"/>s in <see cref="activeQuests"/>.
    /// </summary>
    public void Reset(){
        foreach (Quest quest in activeQuests){
            quest.Reset();
        }
        activeQuests.Clear();
    }

    /// <summary>
    /// Saves data.
    /// </summary>
    internal void Save() {
        User.Save();
    }
    
    /// <summary>
    /// When <see cref="Quest"/> is done, it will save <see cref="Time.timeSinceLevelLoad"/> to <see cref="UserData"/>
    /// base on <see cref="QuestEnum"/>.
    /// </summary>
    /// <param name="quest">which quest was done</param>
    public void QuestDone(QuestEnum quest){
        if (User.breakfastData != null){
            switch (quest) {
                case QuestEnum.Toast:
                    User.breakfastData.toast = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Tea:
                    User.breakfastData.tea = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Coffee:
                    User.breakfastData.coffee = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Omelette:
                    User.breakfastData.omelette = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Bacon:
                    User.breakfastData.bacon = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Juice:
                    User.breakfastData.juice = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Cocoa:
                    User.breakfastData.cocoa = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Milk:
                    User.breakfastData.milk = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                default:
                    break;
            }
        } else if (User.farmData != null){
            switch (quest){
                case QuestEnum.Chicken:
                    User.farmData.chicken = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Horse:
                    User.farmData.horse = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Cow:
                    User.farmData.cow = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Water:
                    User.farmData.water = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Wood:
                    User.farmData.wood = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Carrot:
                    User.farmData.carrot = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Pumpkin:
                    User.farmData.pumpkin = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                default:
                    break;
            }
        } else if (User.houseData != null){
            switch (quest){
                case QuestEnum.Bottle:
                    User.houseData.bottles = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Chairs:
                    User.houseData.chairs = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Cup:
                    User.houseData.cups = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Pillow:
                    User.houseData.pillows = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                case QuestEnum.Soap:
                    User.houseData.soap = Math.Round((double) Time.timeSinceLevelLoad, 2);
                    break;
                default:
                    break;
            }
        }
    }
}
