﻿//============
// Jan Tošner
//============
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
/// <summary>
/// Stores and saves data to json.
/// </summary>
public class UserData : MonoBehaviour {
    public string UserName;
    public DataStorage Storage;
    public BreakfastData breakfastData;
    public FarmData farmData;
    public HouseData houseData;
    private string folder = "default";
    
    /// <summary>
    /// Saves <see cref="Storage"/> to json.
    /// </summary>
    public void Save() {
        DateTime now = DateTime.Now;
        string path = string.Format("{0}/data/{1}/{2}/{3}.json", Application.persistentDataPath, UserName, folder, now.ToString("yyyy-MM-dd--HH-mm-ss", DateTimeFormatInfo.CurrentInfo));
        //Debug.Log(Application.dataPath);
        Storage.time = Math.Round((double) Time.timeSinceLevelLoad, 2);
        //string json = JsonUtility.ToJson(Storage);
        string json = Storage.ToJson();
        EnsureDirectoryExists(string.Format("{0}/data/{1}", Application.persistentDataPath, UserName));
        EnsureDirectoryExists(string.Format("{0}/data/{1}/{2}", Application.persistentDataPath, UserName, folder));
        EnsureDirectoryExists(path);
        StreamWriter sw = File.AppendText(path);
        sw.Write(json);
        sw.Close();
    }

    void Load(string date) {
        string path = string.Format("{0}/{1}/{2}/{3}{4}", Application.persistentDataPath, "data", UserName, date, ".json");
        Debug.Log(path);
        StreamReader sr = new StreamReader(path);
        string json = sr.ReadToEnd();
        Storage = JsonUtility.FromJson<DataStorage>(json);

    }
    private static void EnsureDirectoryExists(string filePath) {
        FileInfo fi = new FileInfo(filePath);
        if (!fi.Directory.Exists) {
            Directory.CreateDirectory(fi.DirectoryName);
        }
    }

    /// <summary>
    /// Creates <see cref="DataStorage"/> based on selected scene.
    /// </summary>
    /// <param name="map">selected button</param>
    /// <exception cref="ArgumentOutOfRangeException">bad map ID</exception>
    public void CreateData(int map){
        Debug.Log("Map: " + map);
        switch (map){
            case 0: //Breakfast
                Storage = new BreakfastData();
                breakfastData = Storage as BreakfastData;
                farmData = null;
                houseData = null;
                folder = "Breakfast";
                break;
            case 1: //Farm
                Storage = new FarmData();
                farmData = Storage as FarmData;
                breakfastData = null;
                houseData = null;
                folder = "Farm";
                break;
            case 2: //House
                Storage = new HouseData();
                houseData = Storage as HouseData;
                breakfastData = null;
                farmData = null;
                folder = "House";
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(map), map, "Unknown map.");
        }
    }

    /// <summary>
    /// Used to store data before saving to json.
    /// </summary>
    [Serializable]
    public class DataStorage {
        public double time;

        public string ToJson(){
            return JsonUtility.ToJson(this);
        }
    }
    
    /// <summary>
    /// <see cref="DataStorage"/> for breakfast scene.
    /// </summary>
    [Serializable]
    public class BreakfastData : DataStorage {
        public double bacon;
        public double cocoa;
        public double coffee;
        public double juice;
        public double milk;
        public double omelette;
        public double tea;
        public double toast;

    }
    
    /// <summary>
    /// <see cref="DataStorage"/> for farm scene.
    /// </summary>
    [Serializable]
    public class FarmData : DataStorage {
        public double carrot;
        public double chicken;
        public double cow;
        public double horse;
        public double pumpkin;
        public double water;
        public double wood;

    }
    
    /// <summary>
    /// <see cref="DataStorage"/> for house scene.
    /// </summary>
    [Serializable]
    public class HouseData : DataStorage {
        public double bottles;
        public double chairs;
        public double cups;
        public double pillows;
        public double soap;
    }
}
