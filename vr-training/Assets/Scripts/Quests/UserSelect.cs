﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.Events;
using UnityEngine.UI;
/// <summary>
/// Handles user selection in GUI.
/// </summary>
public class UserSelect : MonoBehaviour{
    public List<Text> labels;
    public List<Button> buttons;
    
    public Button leftButton;
    public Button rightButton;
    public Text errorText;
    
    public List<string> files;

    public UnityEvent onCreateNewAccount;

    private string _surname;
    private string _name;

    private int _page = 0;
    
    public int Page {
        get { return _page; }
        set {
            if (value <= 0) _page = 0;
            else if (labels.Count * value < files.Count) _page = value;
            UpdateButtons();
        }
    }
    public string Surname{
        get => _surname;
        set{
            _surname = value;
            UpdateList();
        }
    }

    public string Name{
        get => _name;
        set{
            _name = value;
            UpdateList();
        }
    }

    // Start is called before the first frame update
    void Start(){
        EnsureDirectoryExists(string.Format("{0}/data/", Application.persistentDataPath));
        UpdateList();

    }

    private void UpdateList(){
        files.Clear();
        string path = string.Format("{0}/data/", Application.persistentDataPath);
        string [] temp = Directory.GetDirectories(path, _surname+"*_"+_name+"*");
        foreach (var folder in temp){
            files.Add(folder.Substring(path.Length));
        }
        Page = 0;
    }
    
    private static void EnsureDirectoryExists(string filePath) {
        FileInfo fi = new FileInfo(filePath);
        if (!fi.Directory.Exists) {
            Directory.CreateDirectory(fi.DirectoryName);
        }
    }
    
    private void UpdateButtons(){
        if (Page == 0)leftButton.enabled = false;
        else leftButton.enabled = true;
        if (labels.Count * (Page + 1) >= files.Count) rightButton.enabled = false;
        else rightButton.enabled = true;
        for (int i = 0; i < labels.Count; i++) {
            int index = Page * labels.Count + i;
            if (index < files.Count) {
                labels[i].text = files[index].Replace("_"," ");
                buttons[i].gameObject.SetActive(true);
            } else {
                labels[i].text = "";
                buttons[i].gameObject.SetActive(false);
            }
        }
    }
    
    /// <summary>
    /// Page listing.
    /// </summary>
    public void OnButtonLeft() {
        Page--;
    }
    
    /// <summary>
    /// Page listing.
    /// </summary>
    public void OnButtonRight() {
        Page++;
    }

    /// <summary>
    /// Selects user by button pressed.
    /// </summary>
    /// <param name="i">pressed button</param>
    public void SelectUser(int i){
        int index = Page * labels.Count + i;
        QuestManager.instance.User.UserName = files[index];
    }

    /// <summary>
    /// Creates new account and handles errors.
    /// </summary>
    public void CreateAccount(){
        if (string.IsNullOrEmpty(_name) ){
            //errorText.text = "Jméno nesmí být prázdné.";Application.dataPath
            errorText.text = Application.dataPath;
            errorText.gameObject.SetActive(true);
        } else if (string.IsNullOrEmpty(_surname)){
            errorText.text = "Příjmení nesmí být prázdné.";
            errorText.gameObject.SetActive(true);
        } else if (files.Contains(_surname+"_"+_name)){
            errorText.text = "Shodné jméno. Zvolte účet nebo přidejte číslovku.";
            errorText.gameObject.SetActive(true);
        } else{
            QuestManager.instance.User.UserName = _surname + "_" + _name;
            onCreateNewAccount.Invoke();
        }
        
    }

    private void OnEnable(){
        errorText.gameObject.SetActive(false);
    }
}
