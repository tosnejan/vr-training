﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Captures dust and releases it if it is in <code>DustFallAngle</code>.
/// </summary>
public class Spoon : MonoBehaviour
{
    [SerializeField] GameObject dust;
    [SerializeField] GameObject spawned;
    private bool isFilled = false;
    private bool cantFill = false;
    private Renderer dustRen;
    private string dustType;
    [SerializeField] float DustFallAngle = 40.0f;

    // Start is called before the first frame update
    void Start()
    {
        dustRen = dust.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(transform.rotation);
        if((transform.rotation.eulerAngles.x > DustFallAngle && transform.rotation.eulerAngles.x < 360 - DustFallAngle) || (transform.rotation.eulerAngles.z > DustFallAngle && transform.rotation.eulerAngles.z < 360 - DustFallAngle))
        {
            if (isFilled)
            {
                GameObject obj = Instantiate(spawned, transform.position, transform.rotation);
                obj.GetComponent<Renderer>().material = dustRen.material;
                obj.GetComponent<DustType>().dustType = dustType;
                dustRen.enabled = false;
                isFilled = false;
            }

            cantFill = true;
        }
        else
        {
            cantFill = false;
        }
        //Debug.Log(cantFill.ToString());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Sand") && !cantFill)
        {
            setMaterial(other);
            dustType = other.GetComponent<DustType>().dustType;
            isFilled = true;

        }
    }

    private void setMaterial(Collider other)
    {
        Renderer coliderRen = other.GetComponent<Renderer>();
        dustRen.material = coliderRen.material;
        dustRen.enabled = true;
    }
}
