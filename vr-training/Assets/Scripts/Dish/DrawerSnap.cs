﻿//============
// Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Sets this game object transform as parent for <code>DrawerAttachPoint</code>.
/// </summary>
public class DrawerSnap : MonoBehaviour
{
    private List<Collider> registered = new List<Collider>();
    private void OnTriggerEnter(Collider other){
        if (other.gameObject.layer != LayerMask.NameToLayer("PickedUp")){
            DrawerAttachPoint point = GetPoint(other);
            if (point && point.transform != transform){
                point.transform.parent = transform;
                registered.Add(other);
            }
        }
    }
    
    private void OnTriggerStay(Collider other){
        if (registered.Contains(other))return;
        if (other.gameObject.layer != LayerMask.NameToLayer("PickedUp")){
            DrawerAttachPoint point = GetPoint(other);
            if (point && point.transform != transform){
                point.transform.parent = transform;
                registered.Add(other);
                
            }
        }
    }
    
    private void OnTriggerExit(Collider other){
        if (!registered.Contains(other))return;
        DrawerAttachPoint point = GetPoint(other);
        if (point && point.transform != transform){
            point.transform.parent = null;
            registered.Remove(other);
        }
    }
    private DrawerAttachPoint GetPoint(Collider other){
        DrawerAttachPoint lastPoint = other.GetComponentInParent<DrawerAttachPoint>();
        if (lastPoint && lastPoint.transform.parent){
            DrawerAttachPoint currPoint = lastPoint.transform.parent.GetComponentInParent<DrawerAttachPoint>();
            while (currPoint){
                lastPoint = currPoint;
                if (lastPoint.transform.parent){
                    currPoint = lastPoint.transform.parent.GetComponentInParent<DrawerAttachPoint>();
                }
            }
        }
        return lastPoint;
    }
}
