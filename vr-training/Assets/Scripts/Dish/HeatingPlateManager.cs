﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Holds bool which determines if the heating plate is on.
/// </summary>
public class HeatingPlateManager : MonoBehaviour
{
    [SerializeField] public bool IsON;
}
