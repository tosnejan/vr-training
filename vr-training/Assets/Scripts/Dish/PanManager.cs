﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// It is checking if pan is on heating plate. If the plate is heated, then it will cook food on it.
/// </summary>
public class PanManager : MonoBehaviour
{
    [SerializeField] public bool IsHeated;
    [SerializeField] ParticleSystem fog;
    private AudioSource aSource;
    private Color eggCol;
    private bool isPlay;
    private bool setted;
    void Start()
    {
        aSource = GetComponent<AudioSource>();
        fog.enableEmission = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlay && !setted)
        {
            aSource.Play();
            setted = true;
        }
        if(!isPlay)
        {
            aSource.Stop();
            setted = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("HeatingPlate"))
        {
            if (other.GetComponent<HeatingPlateManager>().IsON)
            {
                IsHeated = true;
            }
            else
            {
                IsHeated = false;
            }
        }

        if (other.CompareTag("Bacon"))
        {
            if (IsHeated)
            {
                other.GetComponent<Bacon>().timeToCook -= Time.deltaTime;
                fog.enableEmission = true;
                isPlay = true;
            }
        }

        if (other.CompareTag("Omelette"))
        {
            if (IsHeated)
            {
                other.GetComponent<Omelette>().IsCooking = true;
                fog.enableEmission = true;
                isPlay = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("HeatingPlate"))
        {
            IsHeated = false;
            fog.enableEmission = false;
            isPlay = false;
        }

        if (other.CompareTag("Omelette"))
        {
            other.GetComponent<Omelette>().IsCooking = false;
            fog.enableEmission = false;
            isPlay = false;
        }

        if (other.CompareTag("Bacon"))
        {
            fog.enableEmission = false;
            isPlay = false;
        }
    }
}
