﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Adds water into kettle if water is flowing inside.
/// </summary>
public class KettleWaterTriger : MonoBehaviour
{
    [SerializeField]
    [Range(0.0f, 1.0f)]
    float waterLevel;
    [SerializeField] GameObject waterLevelController;
    [SerializeField] Renderer waterRenderrer;
    [SerializeField] waterMove waterMove;

    // Update is called once per frame
    void Update()
    {
        waterLevel = ((float)(waterMove.waterUnits) / 150.0f);
        if (waterLevel == 0)
        {
            waterRenderrer.enabled = false;
        }
        else
        {
            waterRenderrer.enabled = true;
            waterLevelController.transform.localScale = new Vector3(waterLevelController.transform.localScale.x, waterLevel, waterLevelController.transform.localScale.z);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("WaterObject"))
        {
            Debug.Log("Trigerred");
            if(waterMove.waterUnits < waterMove.maxWaterCapacity) waterMove.waterUnits += 5;
            var wType = other.GetComponent<WaterType>().waterType;
            Destroy(other.gameObject);
        }
    }
}
