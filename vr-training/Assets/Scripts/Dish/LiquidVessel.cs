﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Controls placing items inside fluid holder. Decides on the final material.
/// </summary>
public class LiquidVessel : MonoBehaviour
{
    [SerializeField]
    private float range = 1.0f;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    float waterLevel;
    [SerializeField] GameObject waterLevelController;
    [SerializeField] Material coffeMat, kakaoMat;
    [SerializeField] Renderer waterRenderrer;
    [SerializeField] Color juiceColor, milkColor, coffeColor, kakaoColor, teaColor, coffeMilkColor;

    public Quest cupOfTea;
    public Quest cupOfCoffee;
    public Quest cupOfCocoa;
    public Quest cupOfJuice;
    public Quest cupOfMilk;

    private bool tea, coffe, kakao, juice, milk, water;

    // Start is called before the first frame update
    void Start()
    {
        if (cupOfTea) cupOfTea.Activate();
        if (cupOfCoffee) cupOfCoffee.Activate();
        if (cupOfCocoa) cupOfCocoa.Activate();
        if (cupOfJuice) cupOfJuice.Activate();
        if (cupOfMilk) cupOfMilk.Activate();
    }

    // Update is called once per frame
    void Update()
    {
        if(waterLevel == 0)
        {
            waterRenderrer.enabled = false;
        }
        else
        {
            DestroyAllChild();
            waterRenderrer.enabled = true;
            waterLevelController.transform.localScale = new Vector3(waterLevelController.transform.localScale.x, waterLevelController.transform.localScale.y, waterLevel);
        }
    }

    private void DestroyAllChild()
    {
        int count = transform.childCount;
        for (int i = 0; i< count; i++)
        {
            Destroy(transform.GetChild(0).gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("DustObject")){
            other.transform.parent = transform;
            other.transform.localPosition = RandomPosition(0.0f);
            other.transform.localRotation = Quaternion.Euler(Vector3.zero);
            Rigidbody oRb = other.GetComponent<Rigidbody>();
            if (other.GetComponent<DustType>().dustType == "coffe") coffe = true;
            if (other.GetComponent<DustType>().dustType == "kakao") kakao = true;
            ComputeColor();

            Destroy(oRb);
        }

        if (other.CompareTag("ShugarObject"))
        {
            other.transform.parent = transform;
            other.transform.localPosition = RandomPosition(0.0f);
            other.transform.localRotation = Quaternion.Euler(Vector3.zero);
            Rigidbody oRb = other.GetComponent<Rigidbody>();
     
            Destroy(other.GetComponent<Throwable>());
            Destroy(other.GetComponent<Interactable>());
            Destroy(oRb);
            ComputeColor();
        }

        if (other.CompareTag("Tea"))
        {
            other.transform.parent = transform;
            other.transform.localPosition = RandomPosition(0.3f);
            //other.transform.localRotation = Quaternion.Euler(Vector3.zero);
            Rigidbody oRb = other.GetComponent<Rigidbody>();
            tea = true;
            ComputeColor();

            //oRb.velocity = Vector3.zero;
            //oRb.angularVelocity = Vector3.zero;
            //oRb.isKinematic = true;
            Destroy(other.GetComponent<Throwable>());
            Destroy(other.GetComponent<Interactable>());
            Destroy(oRb);
        }

        if (other.CompareTag("WaterObject"))
        {
            if(waterLevel<1) waterLevel += 0.05f;
            var wType = other.GetComponent<WaterType>().waterType;
            if (wType == "milk") milk = true;
            if (wType == "juice") juice = true;
            if (wType == "water") water = true;
            Destroy(other.gameObject);
            ComputeColor();
        }
    }

    private void ComputeColor()
    {
        if (juice)
        {
            waterRenderrer.material.color = juiceColor;
            if (cupOfJuice) cupOfJuice.Complete();
            return;
        }
        if(coffe && milk)
        {
            waterRenderrer.material.color = coffeMilkColor;
            if (cupOfCoffee) cupOfCoffee.Complete();
            return;
        }
        if(coffe && water)
        {
            waterRenderrer.material.color = coffeColor;
            if (cupOfCoffee) cupOfCoffee.Complete();
        }
        if(milk && kakao || water && kakao)
        {
            waterRenderrer.material.color = kakaoColor;
            if (cupOfCocoa) cupOfCocoa.Complete();
            return;
        }
        if (milk)
        {
            waterRenderrer.material.color = milkColor;
            if (cupOfMilk) cupOfMilk.Complete();
            return;
        }
        if(water && tea)
        {
            waterRenderrer.material.color = teaColor;
            if (cupOfTea) cupOfTea.Complete();
        }
    }

    private Vector3 RandomPosition(float height)
    {
        Vector3 tmp;
        tmp.x = Random.Range(-range, range);
        tmp.y = Random.Range(-range, range);
        tmp.z = height;
        return tmp;
    }
 
}
