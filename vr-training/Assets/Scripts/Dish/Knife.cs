﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Changes knife and toast material.
/// </summary>
public class Knife : MonoBehaviour {
    public Material clear;
    public Material nutella;
    private Renderer renderer;
    MatType mat = MatType.Clear;
    public enum MatType { Clear, Nutella}

    // Start is called before the first frame update
    void Start() {
        renderer = GetComponentInChildren<Renderer>();

    }

    public void SetMaterial(MatType type) {
        switch (type) {
            case MatType.Clear:
                renderer.material = clear;
                mat = MatType.Clear;
                break;
            case MatType.Nutella:
                renderer.material = nutella;
                mat = MatType.Nutella;
                break;
            default:
                renderer.material = clear;
                mat = MatType.Clear;
                break;
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.collider.CompareTag("Toast")) {
            Toast toast = collision.gameObject.GetComponent<Toast>();

            switch (mat) {
                case MatType.Nutella:
                    renderer.material = nutella;
                    toast.Nutella();
                    break;
                default:
                    break;
            }
        }
    }
}
