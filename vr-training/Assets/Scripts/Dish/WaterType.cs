﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Holds water type string.
/// </summary>
public class WaterType : MonoBehaviour
{
    [SerializeField] public string waterType = "water";
}
