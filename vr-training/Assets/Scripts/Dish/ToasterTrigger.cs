﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Roasts toast inside of it.
/// </summary>
public class ToasterTrigger : MonoBehaviour
{
    public Toast toast;
    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Toast")) {
            toast = other.GetComponentInParent<Toast>();
        }
    }
    private void OnTriggerStay(Collider other) {
        if (other.CompareTag("Toast")) {
            toast = other.GetComponentInParent<Toast>();
        }
    }
    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Toast")) {
            toast = null;
        }
    }

    /// <summary>
    /// Called when Toaster timer is done. It will roast the toast inside.
    /// </summary>
    public void Roast() {
        if (toast != null) {
            toast.Roast();
        }
    }
}
