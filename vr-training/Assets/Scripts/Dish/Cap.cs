﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Handles cap snapping to place and preventing liquid to flow.
/// </summary>
public class Cap : MonoBehaviour {
    private Vector3 initPosition;
    private Quaternion initRotation;
    private Vector3 initScale;
    private Rigidbody rb;
    private Collider col;
    public bool PickedUp { get; set; }
    public float magnGlob;
    public float distanceToSnap = 0.07f;
    [SerializeField] GameObject parent;
    [SerializeField] waterMoveWithCap water;
    private bool setted = true;
    private AudioSource aSource;
    
    // Start is called before the first frame update
    void Start()
    {
        initPosition = transform.localPosition;
        initRotation = transform.localRotation;
        initScale = transform.localScale;
        rb = GetComponent<Rigidbody>();
        aSource = GetComponent<AudioSource>();
        col = GetComponentInChildren<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PickedUp && !setted)
        {
            aSource.PlayOneShot(aSource.clip, aSource.volume);
            setted = true;
            transform.parent = null;
        }

        magnGlob = Vector3.Distance(transform.position, parent.transform.position);
        if (setted && !PickedUp && magnGlob < distanceToSnap)
        {
            transform.parent = parent.transform;
            transform.localPosition = initPosition;
            transform.localRotation = initRotation;
            transform.localScale = initScale;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            if(water != null) water.hasCap = true;
            setted = false;
            col.isTrigger = true;
            rb.isKinematic = true;
        }
        else if (setted){
            if (water != null) water.hasCap = false;
        }
    }

}
