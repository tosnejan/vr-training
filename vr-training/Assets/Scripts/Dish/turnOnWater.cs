﻿//============
// Jan Tošner
//============
using NVIDIA.Flex;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Controls water tap.
/// </summary>
public class turnOnWater : MonoBehaviour
{

    [SerializeField] GameObject trigerPrefab;
    [SerializeField] GameObject streamObject;
    [SerializeField] string waterType = "water";
    FlexSourceActor stream;

    private Interactable interactable;
    private bool setted = false;
    bool flow = false;
    private AudioSource aSource;

    // Start is called before the first frame update
    void Start()
    {
        interactable = GetComponent<Interactable>();
        stream = streamObject.GetComponent<FlexSourceActor>();
        aSource = streamObject.GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update()
    {
        if (flow)
        {
            var triger = Instantiate(trigerPrefab, streamObject.transform.position, streamObject.transform.rotation);
            triger.GetComponent<WaterType>().waterType = this.waterType;
        }

        if (!stream.isActive) aSource.Stop();

    }

    private void HandHoverUpdate(Hand hand)
    {
        GrabTypes grabType = hand.GetGrabStarting();

        if (grabType != GrabTypes.None && setted != true)
        {
            stream.isActive = !stream.isActive;
            aSource.Play();
            flow = !flow;
            setted = true;
        }

        if (grabType == GrabTypes.None)
        {
            setted = false;
        }

    }
}
