﻿//============
// Jan Tošner
//============
using NVIDIA.Flex;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Controls water flow. Checks if it has cap on.
/// </summary>
public class waterMoveWithCap : MonoBehaviour
{

    [SerializeField] bool isFilled = false;
    [SerializeField] public bool hasCap = true;
    [SerializeField] GameObject trigerPrefab;
    [SerializeField] GameObject streamObject;
    [SerializeField]
    [Range(1.0f, 90.0f)]
    float angleToStartFlow = 40.0f;
    [SerializeField] int maxWaterCapacity = 100;
    [SerializeField]
    [Range(0, 150)]
    int waterUnits = 0;
    [SerializeField] string waterType = "water";
    [SerializeField] float force = 30.0f;
    FlexSourceActor stream;
    private AudioSource aSource;
    private bool setted = false;
    // Start is called before the first frame update
    void Start()
    {
        stream = streamObject.GetComponent<FlexSourceActor>();
        aSource = streamObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(waterUnits > 0)
        {
            isFilled = true;
        }
        else
        {
            isFilled = false;
        }

        //Debug.Log(transform.localRotation);
        if ((transform.rotation.eulerAngles.x > angleToStartFlow && transform.rotation.eulerAngles.x < 360-90-angleToStartFlow) && isFilled && !hasCap)
        {
            var triger = Instantiate(trigerPrefab, streamObject.transform.position, streamObject.transform.rotation);
            triger.GetComponent<Rigidbody>().AddForce((-streamObject.transform.forward) * force);
            triger.GetComponent<WaterType>().waterType = this.waterType;
            if (!setted) aSource.Play();
            stream.isActive = true;
            --waterUnits;

        }
        else
        {
            stream.isActive = false;
            aSource.Stop();
            setted = false;
        }
        //Debug.Log(cantFill.ToString());
    }
}
