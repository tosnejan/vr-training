﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Handles kettle switch, sound and particles if turned on.
/// </summary>
public class kettleSwitcher : MonoBehaviour
{

    private bool setted = false;
    private Renderer rnd;
    private float timeRemaining;
    private AudioSource aSource;
    [SerializeField] bool timerIsRunning = false;
    [SerializeField] bool OnOff = false;
    [SerializeField] float timer;
    [SerializeField] Material OnMaterial;
    [SerializeField] Material OffMaterial;
    [SerializeField] ParticleSystem fog;
    [SerializeField] Kettle kettle;
    
    // Start is called before the first frame update
    void Start()
    {
        rnd = GetComponent<Renderer>();
        fog.enableEmission = false;
        timeRemaining = timer;
        aSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!kettle.hasPower)
        {
            offKettle();
        }
        if (timerIsRunning)
        {
            if(timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                offKettle();
            }
        }
    }

    private void HandHoverUpdate(Hand hand)
    {
        GrabTypes grabType = hand.GetGrabStarting();
        
        if (grabType != GrabTypes.None && setted != true && kettle.hasPower)
        {
            OnOff = !OnOff;
            setted = true;
            timerIsRunning = true;

            if (OnOff)
            {
                transform.localRotation = Quaternion.Euler(-90f, 90, -90);
                rnd.material = OnMaterial;
                fog.enableEmission = true;
                aSource.PlayOneShot(aSource.clip, aSource.volume);
            }
            else
            {
                offKettle();
            }
        }




        if (grabType == GrabTypes.None)
        {
            setted = false;
        }

    }

    private void resetTimer()
    {
        timerIsRunning = false;
        timeRemaining = timer;
    }

    private void offKettle()
    {
        OnOff = false;
        transform.localRotation = Quaternion.Euler(-75f, 90, -90);
        rnd.material = OffMaterial;
        fog.enableEmission = false;
        aSource.Stop();
        resetTimer();
    }
}
