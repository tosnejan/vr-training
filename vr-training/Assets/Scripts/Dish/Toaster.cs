﻿//============
// Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

/// <summary>
/// Roasts toast when button is down. Also handles position of button.
/// </summary>
public class Toaster : MonoBehaviour {
    public ClickButton clickButton;
    public Interactable interactable;
    public ToasterTrigger t1;
    public ToasterTrigger t2;
    public Transform button;
    private float time = 30;
    private Vector3 upPosition = new Vector3(0.3f, 0.09f, 0f);
    private Vector3 downPosition = new Vector3(0.3f, 0f, 0f);
    private bool roasting = false;
    private AudioSource aSource;
    public AudioClip onClip, offClip;
    void Start() {
        clickButton.onButtonDown.AddListener(OnButtonDown);
        //hoverButton.onButtonDown.AddListener(OnButtonDown);
        aSource = GetComponent<AudioSource>();
}

    private void OnButtonDown(Hand hand) {
        if (!roasting) {
            StartCoroutine(Roast());
        }
    }

    private IEnumerator Roast() {
        roasting = true;
        button.localPosition = downPosition;
        aSource.PlayOneShot(onClip, aSource.volume-0.3f);
        //yield return new WaitForSeconds(0.01f);
        //interactable.highlightOnHover = false;
        yield return new WaitForSeconds(time);
        //interactable.highlightOnHover = true;
        t1.Roast();
        t2.Roast();
        aSource.PlayOneShot(offClip, aSource.volume);
        button.localPosition = upPosition;
        roasting = false;
    }
}
