﻿using System.Collections;
using System.Collections.Generic;
//============
// Jan Tošner
//============
using UnityEngine;
/// <summary>
/// Snaps kettle to power plate.
/// </summary>
public class Kettle : MonoBehaviour
{
    public bool PickedUp { get; set; }
    [SerializeField] public bool hasPower = true;
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!PickedUp && transform.localPosition.magnitude < 0.25f) {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(0,-90,0);
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            rb.constraints = RigidbodyConstraints.FreezeAll;
            hasPower = true;
        }
        else
        {
            hasPower = false;
            rb.constraints = RigidbodyConstraints.None;
        }

    }
}
