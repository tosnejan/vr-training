﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    /// <summary>
    /// Used in <see cref="CustomCircularDrive"/>.
    /// </summary>
    public class CustomLinearMapping : MonoBehaviour{
        
        public float value;
        public float outAngle;
    }
}