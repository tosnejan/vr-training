﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Should snap things to plate. Does not work well...
/// </summary>
public class PlateSnap : MonoBehaviour
{

    public bool IsPicked { get; set; }

    private void OnTriggerEnter(Collider other)
    {
        if (IsPicked)
        {
            if (other.CompareTag("Toast"))
            {
                other.transform.parent.transform.parent = transform;
            }
            if (other.CompareTag("Omelette"))
            {
                other.transform.parent = transform;
            }
            if (other.CompareTag("Bacon"))
            {
                other.transform.parent = transform;
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Toast"))
        {
            other.transform.parent.transform.parent = null;
        }
        if (other.CompareTag("Omelette"))
        {
            other.transform.parent = null;
        }
        if (other.CompareTag("Bacon"))
        {
            other.transform.parent = null;
        }
    }
}
