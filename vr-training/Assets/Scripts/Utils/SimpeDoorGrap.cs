﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve;
using Valve.VR.InteractionSystem;

/// <summary>
/// Old way of using doors. Some prefabs still use it.
/// </summary>
public class SimpeDoorGrap : MonoBehaviour
{

    [SerializeField] private GameObject parent;
    private Interactable interactable;
    private Vector3 scale;
    // Start is called before the first frame update
    void Start()
    {
        interactable = GetComponent<Interactable>();
        scale = transform.localScale;
    }

    private void HandHoverUpdate(Hand hand)
    {
        GrabTypes grabType = hand.GetGrabStarting();
        bool isGrabEnding = hand.IsGrabEnding(gameObject);

        //grap
        if(interactable.attachedToHand == null && grabType != GrabTypes.None)
        {
            hand.AttachObject(gameObject, grabType);
            hand.HoverLock(interactable);
            Rigidbody rb = parent.GetComponent<Rigidbody>();
            rb.isKinematic = false;
        }
        //release
        else if (isGrabEnding)
        {
            hand.DetachObject(gameObject);
            hand.HoverUnlock(interactable);

            transform.position = parent.transform.position;
            transform.rotation = parent.transform.rotation;
            transform.localScale = scale;

            Rigidbody rbhandler = parent.GetComponent<Rigidbody>();
            rbhandler.velocity = Vector3.zero;
            rbhandler.angularVelocity = Vector3.zero;
        }
    }
}
