﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Stops movement of <see cref="_rb"/> when in <see cref="_initRotation"/>
/// </summary>
public class StickyClose : MonoBehaviour
{
    private Rigidbody _rb;
    private Quaternion _initRotation;

    // Start is called before the first frame update
    void Start()
    {
        _initRotation = transform.parent.rotation;
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!_rb.IsSleeping() && transform.parent.rotation == _initRotation)
        {
            _rb.velocity = new Vector3(0,0,0);
            _rb.angularVelocity = new Vector3(0, 0, 0);
            //_rb.isKinematic = true;
        }
    }
}
