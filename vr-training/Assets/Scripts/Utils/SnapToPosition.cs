﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Snaps object to <see cref="snapPosition"/>. Also handles quests.
/// </summary>
public class SnapToPosition : MonoBehaviour{
    public Transform snapPosition;
    public GameObject highlight;
    
    public Quest Quest;
    private bool questDone = false;

    private Interactable interactable;
    private Rigidbody rigidbody;
    private Throwable throwable;
    // Start is called before the first frame update
    void Start(){
        interactable = GetComponent<Interactable>();
        rigidbody = GetComponent<Rigidbody>();
        throwable = GetComponent<Throwable>();
        if (throwable){
            throwable.onPickUp.AddListener(OnPickUp);
            throwable.onDetachFromHand.AddListener(OnDeatach);
        }

        if (snapPosition){
            snapPosition.gameObject.SetActive(false);
        }

        if (Quest){
            Quest.Activate();
        }
            
    }

    // Update is called once per frame
    void Update()
    {
        if (snapPosition == null) return;
        if ((transform.position - snapPosition.position).magnitude < 0.1f){
            if (interactable && interactable.attachedToHand != null) return;
            if (questDone) return;
            questDone = true;
            Quest.Complete();
            if (rigidbody) rigidbody.velocity = Vector3.zero;
            transform.position = snapPosition.position;
            transform.rotation = snapPosition.rotation;
        } else {
            if (questDone){
                questDone = false;
                Quest.UnComplete();
            }
        }
    }

    /// <summary>
    /// Shows highlight.
    /// </summary>
    public void OnPickUp(){
        snapPosition.gameObject.SetActive(true);
    }
    /// <summary>
    /// Hides highlight.
    /// </summary>
    public void OnDeatach(){
        snapPosition.gameObject.SetActive(false);
    }
        
    
#if UNITY_EDITOR
    //-------------------------------------------------------------------------
    /// <summary>
    /// Allows to create <see cref="SnapToPosition.highlight"/>.
    /// </summary>
    [UnityEditor.CustomEditor( typeof( SnapToPosition ) )]
    public class SnapToPositionEditor : UnityEditor.Editor
    {
        //-------------------------------------------------
        // Custom Inspector GUI allows us to click from within the UI
        //-------------------------------------------------
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            SnapToPosition snap = (SnapToPosition)target;
            if ( GUILayout.Button( "Generate position" ) )
            {
                if(snap.snapPosition){
                    DestroyImmediate(snap.snapPosition.gameObject);
                }

                GameObject gameObject = Instantiate(snap.highlight, snap.transform.position, snap.transform.rotation,
                    snap.transform.parent);
                gameObject.name = snap.name + " snap position";
                snap.snapPosition = gameObject.transform;
                Undo.RecordObject(gameObject, "Created object.");
                EditorUtility.SetDirty(gameObject);
            }
        }
    }
#endif
}

