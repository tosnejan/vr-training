﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Plays sound effects.
/// </summary>
public class GrapSoundsManager : MonoBehaviour
{
    private AudioSource aSource;
    [SerializeField] AudioClip baseTap, glassTap, metalTap;


    // Start is called before the first frame update
    void Start()
    {
        aSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Plays <see cref="baseTap"/> sound;
    /// </summary>
    public void PlayBaseTap()
    {
        aSource.PlayOneShot(baseTap, 0.8f);
    }

    /// <summary>
    /// Plays <see cref="glassTap"/> sound;
    /// </summary>
    public void PlayGlassTap()
    {
        aSource.PlayOneShot(glassTap, aSource.volume);
    }

    /// <summary>
    /// Plays <see cref="metalTap"/> sound;
    /// </summary>
    public void PlayMetalTap()
    {
        aSource.PlayOneShot(metalTap, aSource.volume);
    }
}
