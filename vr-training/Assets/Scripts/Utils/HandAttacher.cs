﻿using System;
//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

/// <summary>
/// Attaches hand to <see cref="RightAttachmentPoint"/> and <see cref="LeftAttachmentPoint"/>, when buttons on
/// controller are pressed.
/// </summary>
public class HandAttacher : MonoBehaviour{
    public GameObject RightAttachmentPoint;
    public GameObject LeftAttachmentPoint;
    [EnumFlags]
    [Tooltip( "The flags used to attach this object to the hand." )]
    public Hand.AttachmentFlags attachmentFlags = Hand.AttachmentFlags.VelocityMovement | Hand.AttachmentFlags.TurnOffGravity;
    [Tooltip( "If true, the drive will stay manipulating as long as the button is held down, if false, it will stop if the controller moves out of the collider" )]
    public bool hoverLock = false;
    
    private Interactable interactable;
    private GameObject attachment;
    private SteamVR_Skeleton_Poser poser;
    
    private void Start(){
        interactable = GetComponent<Interactable>();
        if (RightAttachmentPoint){
            poser = RightAttachmentPoint.GetComponent<SteamVR_Skeleton_Poser>();
        } else if (LeftAttachmentPoint){
            poser = LeftAttachmentPoint.GetComponent<SteamVR_Skeleton_Poser>();
        }
    }

    private GrabTypes leftGrabbedWithType;
    private GrabTypes rightGrabbedWithType;
    private GrabTypes grabbedWithType;
    private bool driving;
    private void HandHoverUpdate(Hand hand){
        GrabTypes startingGrabType = hand.GetGrabStarting();
        GrabTypes bestGrabType = hand.GetBestGrabbingType();
        if (hand.handType == SteamVR_Input_Sources.RightHand){
            grabbedWithType = rightGrabbedWithType;
            attachment = RightAttachmentPoint;
        } else { 
            grabbedWithType = leftGrabbedWithType;
            attachment = LeftAttachmentPoint;
        }
        bool isGrabEnding = hand.IsGrabbingWithType(grabbedWithType) == false;

        if (grabbedWithType == GrabTypes.None && startingGrabType != GrabTypes.None){
            if (hand.handType == SteamVR_Input_Sources.RightHand){
                rightGrabbedWithType = startingGrabType;
            } else { 
                leftGrabbedWithType = startingGrabType;
            }
            grabbedWithType = startingGrabType;
            driving = true;

            if (hoverLock) hand.HoverLock(interactable);

            hand.AttachObject(attachment, bestGrabType, attachmentFlags);
            //hand.skeleton.BlendTo(1f,0.1f);
        }
        else if (grabbedWithType != GrabTypes.None && isGrabEnding){
            driving = false;
            if (hand.handType == SteamVR_Input_Sources.RightHand){
                rightGrabbedWithType = GrabTypes.None;
            } else { 
                leftGrabbedWithType = GrabTypes.None;
            }

            if ( hoverLock ) hand.HoverUnlock(interactable);
            
            hand.DetachObject(attachment);
        }
    }
    
}
