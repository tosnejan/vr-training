﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Destroys game object after <see cref="destroyTime"/>.
/// </summary>
public class DestroyDueTime : MonoBehaviour
{
    [SerializeField] float destroyTime = 1.0f;

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, destroyTime);
    }
}
