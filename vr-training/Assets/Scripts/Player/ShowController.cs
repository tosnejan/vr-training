﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Switch between hands and controller.
/// </summary>
public class ShowController : MonoBehaviour {

    public bool showController = false;
    public bool ShowControllerBool {
        get { return showController; }
        set { showController = value; }
    }

    // Update is called once per frame
    void Update() {
        if (showController) {
            Show();
        } else {
            Hide();
        }

    }

    static void Show() {
        foreach (var hand in Player.instance.hands) {
            hand.ShowController();
            hand.HideSkeleton();
        }
    }
    static void Hide() {
        foreach (var hand in Player.instance.hands) {
            hand.HideController();
            hand.ShowSkeleton();
        }

    }
}
