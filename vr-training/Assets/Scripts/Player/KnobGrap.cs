﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve;
using Valve.VR.InteractionSystem;
/// <summary>
/// Used for turning on and off heating plates.
/// </summary>
public class KnobGrap : MonoBehaviour
{

    [SerializeField] private GameObject HeatingPlate;
    [SerializeField] private Material heatMat;
    [SerializeField] private Material coldMat;

    private Interactable interactable;
    private bool turnOn = false;
    private bool setted;
    private AudioSource aSource;
    // Start is called before the first frame update
    void Start()
    {
        interactable = GetComponent<Interactable>();
        aSource = GetComponent<AudioSource>();
    }

    private void HandHoverUpdate(Hand hand)
    {
        GrabTypes grabType = hand.GetGrabStarting();

        if (grabType != GrabTypes.None && setted != true)
        {
            turnOn = !turnOn;
            aSource.PlayOneShot(aSource.clip, aSource.volume);
            setted = true;
        }

        if(grabType == GrabTypes.None)
        {
            setted = false;
        }

        var renderer = HeatingPlate.GetComponent<Renderer>();
        if (turnOn)
        {
            transform.rotation = Quaternion.Euler(0, 0, 45);
            HeatingPlate.GetComponent<HeatingPlateManager>().IsON = true;
            renderer.material = heatMat;
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            HeatingPlate.GetComponent<HeatingPlateManager>().IsON = false;

            renderer.material = coldMat;
        }
    }
}
