﻿//============
// Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Changes layer of object in hand. It prevents from being able to push yourself by collision with your head.
/// </summary>
public class LayerChanger : MonoBehaviour{
    private LayerMask oldLayer;

    private void Start(){
        Throwable throwable;
        if (TryGetComponent(out throwable)){
            throwable.onPickUp.AddListener(OnPickUp);
            throwable.onDetachFromHand.AddListener(OnDeatach);
            
        } else{
            throwable = GetComponentInParent<Throwable>();
            if (throwable){
                throwable.onPickUp.AddListener(OnPickUp);
                throwable.onDetachFromHand.AddListener(OnDeatach);
            }
        }
    }

    /// <summary>
    /// Called when object is picked up.
    /// It will change it's layer to "PickedUp" and will save old layer.
    /// </summary>
    public void OnPickUp(){
        oldLayer = gameObject.layer;
        gameObject.layer = LayerMask.NameToLayer("PickedUp");
    }
    
    /// <summary>
    /// Called when detached.
    /// Restores old layer.
    /// </summary>
    public void OnDeatach(){
        gameObject.layer = oldLayer;
    }

}
