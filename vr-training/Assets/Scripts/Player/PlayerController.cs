﻿//============
// Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
/// <summary>
/// Controls movement based on <see cref="input"/>.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    public SteamVR_Action_Vector2 input;
    public float speed = 200;
    public bool useHand = true;
    private Rigidbody rb;
    private Vector3 finalDirection = Vector3.zero;

    private void Start(){
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update(){
        if (input.axis.magnitude > 0.1f) {
            Vector3 direction;
            if (useHand) {
                direction = Player.instance.leftHand.transform.TransformDirection(new Vector3(input.axis.x, 0, input.axis.y));
            } else {
                direction = Player.instance.hmdTransform.TransformDirection(new Vector3(input.axis.x, 0, input.axis.y));
            }
            //rb.velocity = speed * Time.deltaTime * Vector3.ProjectOnPlane(direction, Vector3.up).normalized + Vector3.up * rb.velocity.y;
            //rb.AddForce(speed * Vector3.ProjectOnPlane(direction, Vector3.up).normalized);
            finalDirection = Vector3.ProjectOnPlane(direction, Vector3.up).normalized;
        } else{
            finalDirection = Vector3.zero;
        }
    }

    private void FixedUpdate(){
        //rb.AddForce(direction * speed);
        rb.MovePosition(transform.position + (finalDirection * speed * Time.fixedDeltaTime));
    }
}
