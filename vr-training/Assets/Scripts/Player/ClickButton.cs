﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Calls <see cref="HandEvent"/> when button is up, down or pressed.
/// </summary>
public class ClickButton : MonoBehaviour {


    private Interactable interactable;
    private bool setted;
    public HandEvent onButtonDown;
    public HandEvent onButtonUp;
    public HandEvent onButtonIsPressed;

    // Start is called before the first frame update
    void Start() {
        interactable = GetComponent<Interactable>();
    }

    private void HandHoverUpdate(Hand hand) {
        GrabTypes grabType = hand.GetGrabStarting();

        if (grabType != GrabTypes.None && !setted) {
            if (onButtonDown != null) {
                onButtonDown.Invoke(hand);
            }
            setted = true;
        } else if (grabType != GrabTypes.None && setted) {
            if (onButtonIsPressed != null) {
                onButtonIsPressed.Invoke(hand);
            }
            setted = false;
        } else if (grabType == GrabTypes.None) {
            if (onButtonUp != null) {
                onButtonUp.Invoke(hand);
            }
            setted = false;
        }
    }

}
