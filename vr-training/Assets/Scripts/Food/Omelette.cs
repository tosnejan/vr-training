﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Changes material of omelette when it is cooking. Also handles quest.
/// </summary>
public class Omelette : MonoBehaviour{
    public Quest quest;
    private Renderer rnd;
    public bool IsCooking;
    private bool setted;
    private bool questDone = false;
    // Start is called before the first frame update
    void Start()
    {
        rnd = GetComponent<Renderer>();
        quest.Activate();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(rnd.material.color.a);
        if (IsCooking && !setted)
        {
            StartCoroutine(CookEgg());
            setted = true;
        }
    }

    private IEnumerator CookEgg()
    {
        while(rnd.material.color.a < 1.0f && IsCooking)
        {
            yield return new WaitForSeconds(1.0f);

            var eggCol = rnd.material.color;
            
            eggCol.a += 0.1f;
            rnd.material.color = eggCol;
        }
        if (!questDone){
            questDone = true;
            quest.Complete();
        }
    }
}
