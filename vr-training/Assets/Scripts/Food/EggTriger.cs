﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Controls flying egg. When it collides it will spawn omelette.
/// </summary>
public class EggTriger : MonoBehaviour
{
    private bool isOnCollision;
    [SerializeField] GameObject omellete;
    private void OnCollisionEnter(Collision collision)
    {
        
        if (!isOnCollision)
        {
            isOnCollision = true;
            Instantiate(omellete, transform.position, omellete.transform.rotation);
            Destroy(this.gameObject);
        }

    }
}
