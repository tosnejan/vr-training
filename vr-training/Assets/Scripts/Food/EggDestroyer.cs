﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Destroys egg if it collides.
/// </summary>
public class EggDestroyer : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField] float maxSpeed;
    [SerializeField] GameObject destroyedEgg;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        
        if(rb.velocity.sqrMagnitude > maxSpeed)
        {
            var obj = Instantiate(destroyedEgg, transform.position, transform.rotation);
            obj.transform.GetChild(0).transform.parent = transform.parent;
            Destroy(this.gameObject);
        }
    }
}
