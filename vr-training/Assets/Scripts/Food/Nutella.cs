﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Sets knife material if it is inside.
/// </summary>
public class Nutella : MonoBehaviour
{

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Knife")) {
            other.GetComponentInParent<Knife>().SetMaterial(Knife.MatType.Nutella);
        }
    }
}
