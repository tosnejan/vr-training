﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Handles toast materials. Also handles quest.
/// </summary>
public class Toast : MonoBehaviour
{
    public Quest toastWithNutella;

    public Material roasted;
    public Material nutella;
    public Rigidbody rb;
    private Renderer renderer;
    private bool isRoasted = false;
    private bool questDone = false;

    private void Start() {
        renderer = GetComponentInChildren<Renderer>();
        if (rb == null) {
            rb = GetComponent<Rigidbody>();
        }
        toastWithNutella.Activate();
        
    }
    /// <summary>
    /// Changes material and animates toast roasting.
    /// </summary>
    public void Roast() {
        renderer.material = roasted;
        rb.AddForce(new Vector3(0,1.5f,0),ForceMode.Impulse);
        isRoasted = true;
    }
    /// <summary>
    /// Changes material and completes quest.
    /// </summary>
    public void Nutella() {
        if (isRoasted) {
            renderer.material = nutella;
            if (!questDone){
                questDone = true;
                toastWithNutella.Complete();
            }
        }
    }
}
