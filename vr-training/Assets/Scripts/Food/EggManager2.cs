﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Destroys egg if it collides.
/// </summary>
public class EggManager2 : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField] float maxSpeed;
    [SerializeField] GameObject eggIn;
    [SerializeField] Material eggOutMat;
    private Vector3 UpVector = new Vector3(0.0f, 1.0f, 0.0f);
    private AudioSource aSource;
    public bool isHeld = false;

    private bool hasEgg = true;
    private bool IsBroken;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        aSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsBroken && hasEgg && Vector3.Angle(UpVector, transform.forward) >= 100)
        {
            Instantiate(eggIn, transform.position - (UpVector / 100), eggIn.transform.rotation);
            hasEgg = false;
            var rndr = transform.GetChild(0).GetComponent<Renderer>();
            rndr.materials[1] = eggOutMat;
            rndr.materials[2] = eggOutMat;
        }
        //Debug.Log(Vector3.Angle(UpVector, transform.forward).ToString());
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (!IsBroken && isHeld && rb.velocity.sqrMagnitude > maxSpeed)
        {
            transform.GetChild(2).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(true);
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(0).transform.parent = null;
            aSource.PlayOneShot(aSource.clip, aSource.volume);
            IsBroken = true;
        }
    }

    /// <summary>
    /// Setter for <code>isHeld</code>.
    /// </summary>
    /// <param name="value">new value</param>
    public void SetHeld(bool value){
        isHeld = value;
    }
}
