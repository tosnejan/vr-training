﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// It is not used. Probably attempt for egg.
/// </summary>
public class OpenEgg : MonoBehaviour
{
    [SerializeField] GameObject eggIn;
    [SerializeField] Material eggOutMat;
    private Vector3 UpVector = new Vector3(0.0f,1.0f,0.0f);

    private bool hasEgg = true;
    public bool IsPicked { get; set; }

    // Update is called once per frame
    void Update()
    {
        if(hasEgg && Vector3.Angle(UpVector, transform.forward) >= 100)
        {
            Instantiate(eggIn, transform.position-(UpVector/10), eggIn.transform.rotation);
            hasEgg = false;
            var rndr = GetComponent<Renderer>();
            rndr.materials[1] = eggOutMat;
            rndr.materials[2] = eggOutMat;
        }
        //Debug.Log(Vector3.Angle(UpVector, transform.forward).ToString());
    }
}
