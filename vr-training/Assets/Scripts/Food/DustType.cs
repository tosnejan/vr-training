﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Holds dust type sting.
/// </summary>
public class DustType : MonoBehaviour
{
    public string dustType;
}
