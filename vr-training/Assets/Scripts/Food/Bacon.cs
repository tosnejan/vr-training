﻿//============
// Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Changes material when cooked. Also handles quest.
/// </summary>
public class Bacon : MonoBehaviour
{
    public float timeToCook = 5;
    [SerializeField] Material cookedBaconMat;
    private bool changed;
    public Quest quest;
    private bool questDone = false;

    private void Start(){
        quest.Activate();
    }

    private void Update()
    {
        if(!changed && timeToCook <= 0.0f)
        {
            GetComponentInChildren<Renderer>().material = cookedBaconMat;
            changed = true;
            if (!questDone){
                questDone = true;
                quest.Complete();
            }
        }
    }
}
