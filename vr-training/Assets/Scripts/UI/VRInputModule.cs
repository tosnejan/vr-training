﻿//============
// Jan Tošner
// Based on VR with Andrew - Vive Pointer (SteamVR 2.2): https://www.youtube.com/playlist?list=PLmc6GPFDyfw85CcfwbB7ptNVJn5BSBaXz.
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;

/// <summary>
/// This is used for using GUI in VR. For more info look follow link in header.
/// </summary>
public class VRInputModule : BaseInputModule
{
    public Camera m_Camera;
    public SteamVR_Input_Sources m_TargetSource;
    public SteamVR_Action_Boolean m_ClickAction;
    public GameObject m_Left;
    public GameObject m_Right;

    private GameObject m_CurrentObject = null;
    private PointerEventData m_Data = null;

    protected override void Awake() {
        base.Awake();

        m_Data = new PointerEventData(eventSystem);
    }

    public override void Process() {
        // Reset data, set camera
        m_Data.Reset();
        m_Data.position = new Vector2(m_Camera.pixelWidth / 2, m_Camera.pixelHeight / 2);

        // Raycast
        eventSystem.RaycastAll(m_Data, m_RaycastResultCache);
        m_Data.pointerCurrentRaycast = FindFirstRaycast(m_RaycastResultCache);
        m_CurrentObject = m_Data.pointerCurrentRaycast.gameObject;

        // Clear 
        m_RaycastResultCache.Clear();

        // Hover
        HandlePointerExitAndEnter(m_Data, m_CurrentObject);

        // Press
        if (m_ClickAction.GetStateDown(m_TargetSource)) {
            ProcessPress(m_Data);
        }
        // Release
        if (m_ClickAction.GetStateUp(m_TargetSource)) {
            ProcessRelease(m_Data);
        }
    }

    public PointerEventData GetData() {
        return m_Data;
    }

    private void ProcessPress(PointerEventData data) {
        // Set raycast
        data.pointerCurrentRaycast = data.pointerCurrentRaycast;

        // Check for object hit, get the down handler, call
        GameObject newPointerPress = ExecuteEvents.ExecuteHierarchy(m_CurrentObject, data, ExecuteEvents.pointerDownHandler);

        // If no down handler, try and get click handler
        if (newPointerPress == null) {
            newPointerPress = ExecuteEvents.GetEventHandler<IPointerClickHandler>(m_CurrentObject);
        }

        // Set data
        data.pressPosition = data.position;
        data.pointerPress = newPointerPress;
        data.rawPointerPress = m_CurrentObject;

    }

    private void ProcessRelease(PointerEventData data) {
        // Execute pointer up
        ExecuteEvents.Execute(data.pointerPress, data, ExecuteEvents.pointerUpHandler);

        // Check dor click handler
        GameObject pointerUpHandler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(m_CurrentObject);

        // Check if actual
        if (data.pointerPress == pointerUpHandler) {
            ExecuteEvents.Execute(data.pointerPress, data, ExecuteEvents.pointerClickHandler);
        }

        // Clear selected gameobject
        eventSystem.SetSelectedGameObject(null);

        // Reset data
        data.pressPosition = Vector2.zero;
        data.pointerPress = null;
        data.rawPointerPress = null;

    }

    public void UseLeft() {
        m_Camera = m_Left.GetComponent<Camera>();
        m_TargetSource = SteamVR_Input_Sources.LeftHand;
        m_Left.SetActive(true);
        m_Right.SetActive(false);
        foreach (var canvas in FindObjectsOfType<Canvas>()) {
            canvas.worldCamera = m_Camera;
        }
    }
    public void UseRight() {
        m_Camera = m_Right.GetComponent<Camera>();
        m_TargetSource = SteamVR_Input_Sources.RightHand;
        m_Right.SetActive(true);
        m_Left.SetActive(false);
        foreach (var canvas in FindObjectsOfType<Canvas>()) {
            canvas.worldCamera = m_Camera;
        }
    }
    public void TurnOff() {
        m_Right.SetActive(false);
        m_Left.SetActive(false);
    }
    public void SwapHands() {
        if (m_Right.activeSelf) {
            UseLeft();
        } else {
            UseRight();
        }
    }

}
