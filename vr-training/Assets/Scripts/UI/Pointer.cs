﻿//============
// Jan Tošner
// Based on VR with Andrew - Vive Pointer (SteamVR 2.2): https://www.youtube.com/playlist?list=PLmc6GPFDyfw85CcfwbB7ptNVJn5BSBaXz.
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// This is used for using GUI in VR. For more info look follow link in header.
/// </summary>
public class Pointer : MonoBehaviour
{
    public float m_DefaultLength = 5.0f;
    public GameObject m_Dot;
    public VRInputModule m_InputModule;

    private LineRenderer m_LineRenderer = null;

    void Awake(){
        m_LineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update(){
        UpdateLine();
    }

    private void UpdateLine() {
        // Use default or distance
        PointerEventData data = m_InputModule.GetData();
        float targetLength = data.pointerCurrentRaycast.distance == 0 ? m_DefaultLength : data.pointerCurrentRaycast.distance;

        // Raycast
        RaycastHit hit = CreateRaycast(targetLength);

        // Default
        Vector3 endPosition = transform.position + (transform.forward * targetLength);

        // Or based on hit
        if (hit.collider != null) {
            endPosition = hit.point;
        }

        // Set position of the dot
        m_Dot.transform.position = endPosition;

        // Set linerenderer
        m_LineRenderer.SetPosition(0, transform.position);
        m_LineRenderer.SetPosition(1, endPosition);
    }

    private RaycastHit CreateRaycast(float length) {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);
        Physics.Raycast(ray, out hit, m_DefaultLength);
        return hit;
    }
}
