﻿//============
// Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;
/// <summary>
/// Switches scenes with <see cref="SteamVR_LoadLevel"/>.
/// </summary>
public class SceneSwitcher : MonoBehaviour{
    //public Texture texture;
    private SteamVR_LoadLevel _loadLevel;

    private void Start(){
        _loadLevel = GetComponent<SteamVR_LoadLevel>();
    }

    /// <summary>
    /// Switches scenes with <see cref="SteamVR_LoadLevel"/>.
    /// </summary>
    public void ChangeScene(string scene = "Menu") {
        if (SceneManager.GetSceneByName(scene) == null) return;
        if (_loadLevel){
            _loadLevel.levelName = scene;
            _loadLevel.Trigger();
        } else {
            SteamVR_LoadLevel.Begin(scene, true);
        }


    }

}
