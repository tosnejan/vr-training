﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
/// <summary>
/// Keyboard in VR.
/// </summary>
public class Keyboard : MonoBehaviour{
    private InputField _inputField;

    private GameObject keyboard;
    // Start is called before the first frame update
    void Start(){
        keyboard = transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (EventSystem.current.currentSelectedGameObject != null ){
            if (EventSystem.current.currentSelectedGameObject.TryGetComponent(out InputField field)){
                _inputField = field;
                keyboard.SetActive(true);
            } else if (keyboard.transform.parent && !EventSystem.current.currentSelectedGameObject.transform.IsChildOf(keyboard.transform.parent)){
                //_inputField = null;
                if (keyboard.activeSelf){
                    keyboard.SetActive(false);
                }
            }
        }
    }

    /// <summary>
    /// Adds <paramref name="letter"/> to the end of string. Handles first big letter.
    /// </summary>
    /// <param name="letter">letter to write</param>
    public void TextInput(string letter){
        _inputField.text += _inputField.text.Length == 0 ? letter[0] : char.ToLower(letter[0]);
    }

    /// <summary>
    /// Deletes last letter.
    /// </summary>
    public void Backspace(){
        if (_inputField.text.Length == 0) return;
        _inputField.text = _inputField.text.Remove(_inputField.text.Length - 1);
    }
}
