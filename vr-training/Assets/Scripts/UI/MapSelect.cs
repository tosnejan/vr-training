﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Selects map.
/// </summary>
public class MapSelect : MonoBehaviour
{
    /// <summary>
    /// Selects map and resets quests.
    /// </summary>
    public void CreateData(int map){
        QuestManager.instance.Reset();
        QuestManager.instance.User.CreateData(map);
    }
}
