﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Menu that is used to show active quests.
/// </summary>
public class PortableMenu : MonoBehaviour
{
    public GameObject child;
    public BoxCollider col;
    public VRInputModule m_InputModule;
    public Canvas canvas;

    public Hand hand;
    //private MeshRenderer _renderer;
    void Start() {
        //_renderer = GetComponent<MeshRenderer>();
        //_renderer.enabled = false;
        if (col == null) {
            col = GetComponent<BoxCollider>();
        }
        col.enabled = true;
        if (child == null) {
            child = transform.GetChild(0).gameObject;
        }
        child.SetActive(false);
        if (m_InputModule == null) {
            Debug.LogError("Missing VRInputModule");
        }
        if (canvas == null) {
            canvas = GetComponentInChildren<Canvas>();
        }
    }
    /// <summary>
    /// Called on pick up. Determines which hand to use for pointer.
    /// </summary>
    public void OnPickUp() {
        //_renderer.enabled = true;
        hand = GetComponentInParent<Hand>();
        child.SetActive(true);
        col.enabled = false;
        if (hand.handType == Valve.VR.SteamVR_Input_Sources.LeftHand) {
            m_InputModule.UseRight();
        } else {
            m_InputModule.UseLeft();
        }
        canvas.worldCamera = m_InputModule.m_Camera;
    }
    /// <summary>
    /// Called when detached. Turns off <see cref="VRInputModule"/>.
    /// </summary>
    public void OnDetach() {
        //_renderer.enabled = false;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        child.SetActive(false);
        col.enabled = true;
        m_InputModule.TurnOff();

    }
    
    /// <summary>
    /// Detaches this game object when switching scenes.
    /// </summary>
    public void OnExit(){
        hand.DetachObject(gameObject);
    }
}
