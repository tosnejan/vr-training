﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles button in GUI.
/// </summary>
public class QuestMenu : MonoBehaviour
{
    public List<Text> labels;
    public List<Toggle> toggles;

    public Button leftButton;
    public Button rightButton;
    private int page = 0;
    public int Page {
        get { return page; }
        set {
            if (value <= 0) page = 0;
            else if (labels.Count * value < QuestManager.instance.activeQuests.Count) page = value;
        }
    }

    void Update() {
        if (QuestManager.instance == null) return;
        if (Page == 0)leftButton.gameObject.SetActive(false);
        else leftButton.gameObject.SetActive(true);
        if (labels.Count * (Page + 1) >= QuestManager.instance.activeQuests.Count) rightButton.gameObject.SetActive(false);
        else rightButton.gameObject.SetActive(true);
        for (int i = 0; i < labels.Count; i++) {
            int index = Page * labels.Count + i;
            if (index < QuestManager.instance.activeQuests.Count) {
                labels[i].text = QuestManager.instance.activeQuests[index].message;
                if (QuestManager.instance.activeQuests[index].countToBeDone != 1){
                    labels[i].text += " (" + QuestManager.instance.activeQuests[index].CurrentlyDone + "/" +
                                      QuestManager.instance.activeQuests[index].countToBeDone + ")";
                } 
                toggles[i].isOn = QuestManager.instance.activeQuests[index].done;
                toggles[i].gameObject.SetActive(true);
            } else {
                labels[i].text = "";
                toggles[i].isOn = false;
                toggles[i].gameObject.SetActive(false);
            }
        }

    }

    /// <summary>
    /// Listing.
    /// </summary>
    public void OnButtonLeft() {
        Page--;
    }
    
    /// <summary>
    /// Listing.
    /// </summary>
    public void OnButtonRight() {
        Page++;

    }
    /// <summary>
    /// Saves data.
    /// </summary>
    public void Save() {
        QuestManager.instance.Save();
    }
}
