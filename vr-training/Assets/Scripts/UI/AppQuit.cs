﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Quits app.
/// </summary>
public class AppQuit : MonoBehaviour
{
    /// <summary>
    /// Quits app.
    /// </summary>
    public void Quit() {
        Application.Quit();
    }
}
