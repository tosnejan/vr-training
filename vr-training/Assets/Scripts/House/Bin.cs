﻿//============
// Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Completes quest and destroys trash.
/// </summary>
public class Bin : MonoBehaviour
{

    private void OnTriggerStay(Collider other){
        if (other.TryGetComponent(out Trash trash)){
            if (other.gameObject.layer == LayerMask.NameToLayer("PickedUp")) return;
            if (trash.Quest){
                trash.Quest.Complete();
            }
            Destroy(trash.transform.parent.gameObject);
        }
    }
}
