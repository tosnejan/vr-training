﻿//============
// Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Handles showering quest.
/// </summary>
public class Soap : MonoBehaviour{
    public Quest Quest;

    private bool _pickedUp;
    private bool questDone;

    public bool PickedUp{
        get => _pickedUp;
        set => _pickedUp = value;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (Quest){
            Quest.Activate();
        }
    }

    private void OnTriggerStay(Collider other){
        if (questDone || !_pickedUp) return;
        if (other.CompareTag("BodyTrigger")){
            questDone = true;
            if (Quest){
                Quest.Complete();
            }
        }
    }
}
