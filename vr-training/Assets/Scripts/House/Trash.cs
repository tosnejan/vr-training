﻿//============
// Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Activates quest for trash. Also is used as Quest handler for calling Complete from Bin.
/// </summary>
public class Trash : MonoBehaviour {
    
    public Quest Quest;
    // Start is called before the first frame update
    void Start(){
        if (Quest){
            Quest.Activate();
        }
    }
}
