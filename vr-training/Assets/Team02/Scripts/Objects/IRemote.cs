﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
public interface IRemote
{
    void Interact(GameObject controller);
}
