﻿//============
// Edit: Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
/// <summary>
/// Fills bucket under with milk and changes color based on distance.
/// </summary>
public class Cecek : MonoBehaviour
{

    private bool _holded = false;
    private Hand _hand;
    private float _grabPlace;
    private float _distance;
    private float _lastDistance;
    [SerializeField] Cow cow;
    [SerializeField] private float maxDistance = 5;
    [SerializeField] private UnderCow undercow;
    private Interactable _interactable;
    private Throwable _throwable;
    public GameObject collider;

    private void Start(){
        _interactable = GetComponentInChildren<Interactable>();
        _throwable = GetComponentInChildren<Throwable>();
    }

    /// <summary>
    /// Fills bucket under with milk and changes color based on distance.
    /// </summary>
    public void OnHeldUpdate()
    {
        if (!_hand){
            _hand = _interactable.attachedToHand;
            if (!_hand)return;
        }
        float currDist = Vector3.Distance(_hand.transform.position, transform.position);
        GetComponentInChildren<Renderer>().material.color = new Color((1 / maxDistance)*(currDist-_distance), 0, 0);
        if (currDist - _distance > maxDistance)
        {
            //Debug.Log("OUCH IT HURTS");
            //_controller.GetComponent<Hold>().UnHold(this.gameObject);
            cow.PlaySound();
            OnDetach();
            return;
        }
        if (Mathf.Abs(currDist - _lastDistance) > 0.01)
        {
            if (currDist > _lastDistance && undercow.GetBucketOn())
            {
                Bucket bucket = undercow.GetBucket();
                if (bucket.IsEmpty() || !bucket.IsWater())
                {
                    bucket.SetMilk();
                    //Debug.Log(currDist - _lastDistance*100);
                    bucket.addFulness((currDist - _lastDistance)*100);
                }
            }
            _lastDistance = Vector3.Distance(_hand.transform.position, transform.position);
        }
    }
    /// <summary>
    /// Disables collider renderer which is used for highlight but should not be seen when held.
    /// </summary>
    public void OnPickUp()    {
        //Debug.Log("Grabed cecek");
        Hand hand = _interactable.attachedToHand;
        _distance = Vector3.Distance(hand.transform.position, transform.position);
        _hand = hand;
        _lastDistance = _distance;
        collider.GetComponentInChildren<Renderer>().enabled = false;
    }

    /// <summary>
    /// Enables collider renderer which is used for highlight but should not be seen when held.
    /// </summary>
    public void OnDetach()
    {
        if (_interactable.attachedToHand == _hand){
            _hand.DetachObject(_interactable.gameObject);
        }
        collider.transform.localPosition = Vector3.zero;
        collider.transform.localRotation = Quaternion.identity;
        _hand = null;
        GetComponentInChildren<Renderer>().material.color = new Color(1,1,1);
        collider.GetComponentInChildren<Renderer>().enabled = true;
    }

    public bool IsBeingHold()
    {
        if (_hand == null)
        {
            return false;
        }
        return true;
    }
}
