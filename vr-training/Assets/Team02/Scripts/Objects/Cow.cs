﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Plays sound of cow.
/// </summary>
public class Cow : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Plays audioSource sound.
    /// </summary>
    public void PlaySound() {
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }
}
