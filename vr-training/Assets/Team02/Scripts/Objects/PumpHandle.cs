﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// Adds water to bucket when on drain and pump is being pumped.
/// </summary>
public class PumpHandle : MonoBehaviour, IGrabbable
{
    [SerializeField]
    private GameObject _controller;
    [SerializeField]
    private bool _grabbed = false;
    [SerializeField]
    private Vector3 _controlPosition;
    public delegate void Action(float a);
    public Action rotAction;
    [SerializeField]
    private Drain drain;

    private float lastAngle;

    /// <summary>
    /// Not used.
    /// </summary>
    public void Grabbed(GameObject controller)
    {
        Debug.Log("Handle grabbed");
        if (!_grabbed)
        {
            _grabbed = true;
            _controller = controller;
            _controlPosition = controller.transform.position;
        }
    }
    /// <summary>
    /// Not used.
    /// </summary>
    public void Dropped()
    {
        _grabbed = false;
        _controller = null;
    }
    /// <summary>
    /// Not used.
    /// </summary>y
    public bool IsGrabbed()
    {
        if (_controller == null)
        {
            return false;
        }
        return true;
    }
    /// <summary>
    /// Adds water to bucket when on drain.
    /// </summary>
    public void OnMove(){
        if (lastAngle > transform.localRotation.z){
            if (drain.GetBucketOn()){
                Bucket bucket = drain.GetBucket();
                if (bucket.IsEmpty() || bucket.IsWater()){
                    bucket.SetWater();
                    bucket.addFulness(((int) transform.localRotation.z - lastAngle) * 5);
                }
            }
        }
        lastAngle = transform.localRotation.z;
    }

}
