﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Sets bowl in chicken enclosure.
/// </summary>
public class ChickenEnclosure : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Miska")
        {
            other.gameObject.GetComponent<Bowl>().SetInChickenEnclosure(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
         if (other.gameObject.name == "Miska")
        {
            other.gameObject.GetComponent<Bowl>().SetInChickenEnclosure(false);
        }

    }
}
