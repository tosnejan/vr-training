﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Sets bowl full on trigger enter.
/// </summary>
public class Sack : MonoBehaviour
{
    [SerializeField] private GameObject notebook;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Miska")
        {
            Bowl bowl = other.gameObject.GetComponent<Bowl>();
            if (!bowl.GetIsFull())
            {
                bowl.SetIsFull(true);
                notebook.SendMessage("UpdateTask", 5);
            }    

        }
    }
}
