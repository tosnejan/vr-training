﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Controls water level and also handles quest.
/// </summary>
public class waterholder : MonoBehaviour
{
    [SerializeField]
    private float waterLevel = 0;
    private Renderer rend;
    [SerializeField]
    private GameObject notebook;
    public Quest waterHolder;

    public void Start()
    {
         rend = GetComponent<Renderer>();
         waterHolder.Activate();
    }

    public void AddWaterLevel(float value) {
        waterLevel += value;
        if (waterLevel >= 60)
        {
            waterLevel = 60;
            notebook.SendMessage("UpdateTask", 2);
            waterHolder.Complete();
            rend.material.color = new Color(0, 1, 0);
        }
        else
        {
            rend.material.color = new Color(0, 0, waterLevel / 60);
        }       
    }

}
