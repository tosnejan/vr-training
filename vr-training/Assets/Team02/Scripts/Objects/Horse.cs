﻿//============
// Edit: Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Plays sounds of Horse. Also handles quest.
/// </summary>
public class Horse : MonoBehaviour
{
    [SerializeField] private AudioSource horseAudio;
    [SerializeField] private GameObject notebook;
    public Quest brushHorse;

    private void Start(){
        brushHorse.Activate();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Brush"))
        {
            if (!horseAudio.isPlaying)
            {
                horseAudio.Play();
            }
            notebook.SendMessage("UpdateTask", 7);
            brushHorse.Complete();
        }
    }
}
