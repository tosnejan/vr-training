﻿//============
// Edit: Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Changes materials and also handles quest.
/// Some of the functions are remains from old project and they are not used.
/// </summary>
public class Bucket : MonoBehaviour, IGrabbable
{
    [SerializeField]
    private float fullness = 0;
    [SerializeField]
    private bool isAboveWaterHolder = false;
    private waterholder _waterholder;
    private bool _grabbed = false;
    private GameObject _controller;
    private bool _isWater;
    [SerializeField]
    private GameObject notebook;
    private bool wasfulled = false;
    private Renderer _renderer;
    public Quest milkBucket;

    private void Start(){
        _renderer = GetComponentInChildren<Renderer>();
        milkBucket.Activate();
    }


    private void Update()
    {
        float x = 0;
        float z = 0;
        float beforeful = fullness;
        if (fullness > 0)
        {
            x = transform.eulerAngles.x;
            z = transform.eulerAngles.z;
            if (x > 180)
            {
                x -= 360;
            }
            if (z > 180)
            {
                z -= 360;
            }
        }
        if (Mathf.Abs(x) > 90 || Mathf.Abs(z) > 90)
        {
            if (fullness > 0)
            {
                fullness = 0;
            }
        }

        else if (Mathf.Abs(x) > 50 || Mathf.Abs(z) > 50 )
        {
            if (fullness > 40)
            {
                fullness -= 5;
            }

        }
        else if (Mathf.Abs(x) > 30 || Mathf.Abs(z) > 30 )
        {
            if (fullness > 60)
            {
                fullness -= 5;
            }
        }
        else if (Mathf.Abs(x) > 20 || Mathf.Abs(z) > 20) 
        {
            if (fullness > 80)
            {
                fullness -= 5;
            }
        }
        if (!wasfulled)
        {
            float newcol = (float)fullness / 100;
            if (_isWater)
            {
                _renderer.material.color = new Color(0, 0, newcol);
            }
            else
            {
                _renderer.material.color = new Color(newcol, newcol, newcol);
            }
        }
        wasfulled = false;
        if (isAboveWaterHolder && _isWater) {
            _waterholder.AddWaterLevel((beforeful - fullness) / 2f);
        }
    }

    /// <summary>
    /// How full is the bucket?
    /// </summary>
    /// <returns>0-100</returns>
    public float getFulness() {
        return fullness;
    }

    /// <summary>
    /// Adds to fullness.
    /// </summary>
    /// <param name="amount">How much to add.</param>
    public void addFulness(float amount) {
        fullness += amount;
        if (fullness > 100) {
            wasfulled = true;
            fullness = 100;
            if (!_isWater) {
                notebook.SendMessage("UpdateTask", 3);
                milkBucket.Complete();
                
            }
            _renderer.material.color = new Color(0, 1, 0);
        }
    }

    public void setFulness(float amount) {
        fullness = amount;
    }
    /// <summary>
    /// Not used.
    /// </summary>
    public void Grabbed(GameObject controller)
    {
        Debug.Log("Grabed bucket");
        if (!_grabbed)
        {
            transform.SetParent(controller.transform);
            _grabbed = true;
            _controller = controller;
            GetComponent<Rigidbody>().isKinematic = true;
        }
    }
    /// <summary>
    /// Not used.
    /// </summary>
    public void Dropped()
    {
        transform.SetParent(null);
        _grabbed = false;
        _controller = null;
        GetComponent<Rigidbody>().isKinematic = false;
    }

    public bool IsGrabbed()
    {
        if (_controller == null)
        {
            return false;
        }
        return true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<waterholder>() != null) {
            isAboveWaterHolder = true;
            _waterholder = other.GetComponent<waterholder>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<waterholder>() != null) {
            isAboveWaterHolder = false;
            _waterholder = null;
        }
    }


    /// <summary>
    /// <code>true</code> if is water.
    /// </summary>
    public bool IsWater() {
        return _isWater;
    }

    /// <summary>
    /// Sets _isWater to <code>true</code>.
    /// </summary>
    public void SetWater() {
        _isWater = true;
    }

    /// <summary>
    /// Sets _isWater to <code>false</code>.
    /// </summary>
    public void SetMilk() {
        _isWater = false;
    }
    public bool IsEmpty() {
        if (fullness == 0)
        {
            return true;
        }
        else {
            return false;
        }
    }

}
