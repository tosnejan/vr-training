﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class Radio : Grabbable, IRemote
{
    [SerializeField] private AudioSource audioData;
    [SerializeField] private GameObject notebook;
    private bool isActive = false;

    public void Interact(GameObject controller)
    {
        if (!isActive)
        {
            isActive = true;
            audioData.Play();
            notebook.SendMessage("UpdateTask", 1);

        }
        else
        {
            isActive = false;
            audioData.Stop();
        }
    }


}
