﻿//============
// Edit: Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Checks for bucket. Holds it's reference.
/// </summary>
public class Drain : MonoBehaviour
{
    [SerializeField]
    private bool bucketOn = false;
    private Bucket bucket;

    private void OnTriggerEnter(Collider other){
        if (other.GetComponentInParent<Bucket>() != null) {
            bucketOn = true;
            bucket = other.gameObject.GetComponentInParent<Bucket>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponentInParent<Bucket>() != null) {
            bucketOn = false;
            bucket = null;
        }
    }

    /// <summary>
    /// Bucket state.
    /// </summary>
    /// <returns><code>true</code> if bucket is on.</returns>
    public bool GetBucketOn() {
        return bucketOn;
    }
    /// <summary>
    /// Getter for bucket.
    /// </summary>
    /// <returns>Bucket which is on drain.</returns>
    public Bucket GetBucket() {
        return bucket;
    }
}
