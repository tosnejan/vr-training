﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
public class Drum : MonoBehaviour, IGrabbable
{
    private bool _grabbed = false;
    private GameObject _controller;
    [SerializeField] private Collider coltrig;
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Dropped()
    {
        transform.SetParent(null);
        _grabbed = false;
        _controller = null;
        GetComponent<Rigidbody>().isKinematic = false;

    }

    public void Grabbed(GameObject controller)
    {
        Debug.Log("Grabed bucket");
        if (!_grabbed)
        {
            transform.SetParent(controller.transform);
            _grabbed = true;
            _controller = controller;
            GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    public bool IsGrabbed()
    {
        return _grabbed;
    }

    private void OnTriggerEnter(Collider other)
    {

                audioSource.Play();

    }
}
