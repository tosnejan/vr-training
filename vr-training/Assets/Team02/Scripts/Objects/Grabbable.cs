﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
public class Grabbable : MonoBehaviour,IGrabbable
{
    public GameObject controller;

    public virtual void Dropped()
    {
        transform.SetParent(null);
        //controller.GetComponent<MeshRenderer>().enabled = true;
        controller = null;
        GetComponent<Rigidbody>().isKinematic = false;
    }

    public virtual void Grabbed(GameObject controller)
    {
        this.controller = controller;
        transform.SetParent(controller.transform);
        //controller.GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
    }

    public bool IsGrabbed()
    {
        if (controller == null) {
            return false;
        }
        return true;
    }
}
