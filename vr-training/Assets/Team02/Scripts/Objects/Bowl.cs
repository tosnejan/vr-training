﻿//============
// Edit: Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Used to activate grain. Also handles quest.
/// Some of the functions are remains from old project and they are not used.
/// </summary>
[RequireComponent(typeof(GameObject))]
public class Bowl : Grabbable{
    public Quest feedChicken;
    [SerializeField] private GameObject grain;
    [SerializeField] private GameObject notebook;
    private bool InChickenEnclosure;
    private bool grabbed;

    private void Start(){
        feedChicken.Activate();
    }


    /// <summary>
    /// Not used.
    /// </summary>
    public override void Dropped()
    {
        base.Dropped();
        if (GetIsFull() && InChickenEnclosure){
            notebook.SendMessage("UpdateTask", 6);
            feedChicken.Complete();
        }
    }


    /// <summary>
    /// Not used.
    /// </summary>
    /// <param name="controller"></param>
    public override void Grabbed(GameObject controller)
    {
        notebook.SendMessage("UpdateTask", 4);
        base.Grabbed(controller);
    }
    
    /// <summary>
    /// Called when bowl is picked up.
    /// </summary>
    public void OnPickUP(){
        grabbed = true;
        notebook.SendMessage("UpdateTask", 4);
    }

    /// <summary>
    /// Called when bowl is detached.
    /// </summary>
    public void OnDeatach(){
        grabbed = false;
        if (GetIsFull() && InChickenEnclosure){
            notebook.SendMessage("UpdateTask", 6);
            feedChicken.Complete();
        }
    }

    /// <summary>
    /// Has grain?
    /// </summary>
    /// <returns><code>true</code> if it has grain.</returns>
    public bool GetIsFull()
    {
        return grain.activeSelf;
    }

    /// <summary>
    /// Toggles grain.
    /// </summary>
    /// <param name="active"><code>true</code> for active, <code>false</code> otherwise.</param>
    public void SetIsFull(bool active)
    {
        grain.SetActive(active);
    }

    /// <summary>
    /// Called when interacting with chicken enclosure.
    /// </summary>
    /// <param name="inside">if is inside</param>
    public void SetInChickenEnclosure(bool inside)
    {
        InChickenEnclosure = inside;
        if (GetIsFull() && !grabbed){
            notebook.SendMessage("UpdateTask", 6);
            feedChicken.Complete();
        }
    }

}
