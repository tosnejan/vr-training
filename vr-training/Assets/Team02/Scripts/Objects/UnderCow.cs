﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Checks for bucket. Holds it's reference.
/// </summary>
public class UnderCow : MonoBehaviour
{
    [SerializeField]
    private bool bucketOn = false;
    private Bucket bucket;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform.GetComponentInParent<Bucket>() != null)
        {
            bucketOn = true;
            bucket = collision.transform.gameObject.GetComponentInParent<Bucket>();
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.transform.GetComponentInParent<Bucket>() != null)
        {
            bucketOn = false;
            bucket = null;
        }
    }

    /// <summary>
    /// Bucket state.
    /// </summary>
    /// <returns><code>true</code> if bucket is on.</returns>
    public bool GetBucketOn()
    {
        return bucketOn;
    }

    /// <summary>
    /// Getter for bucket.
    /// </summary>
    /// <returns>Bucket which is on drain.</returns>
    public Bucket GetBucket()
    {
        return bucket;
    }
}
