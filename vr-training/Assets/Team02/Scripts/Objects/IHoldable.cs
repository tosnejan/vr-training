﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
public interface IHoldable 
{
    void Hold(GameObject controller);
    void Dropped();
    bool IsBeingHold();
}
