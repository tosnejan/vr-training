﻿//============
// Edit: Jan Tošner
//============
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
public class Notebook : MonoBehaviour
{
    List<FarmTask> tasks;

    void Start()
    {
        tasks = new List<FarmTask>()
        {
            new FarmTask("Úkoly:", 0),
            new FarmTask("Zapni rádio", 1),
            new FarmTask("Naplň nádrž ve stodole vodou", 2),
            new FarmTask("Postav kýbl pod krávu a podoj ji", 3),
            new FarmTask("Vezmi misku", 4),
            new FarmTask("Naplň misku obilím", 5),
            new FarmTask("Dej misku slepicím", 6),
            new FarmTask("Vyhřebelcuj koně", 7)
        };
        UpdateText();
    }
    public void UpdateTask(int delTaskNumber)
    {
        tasks[delTaskNumber].Done = true;
        UpdateText();
    }

    private void UpdateText()
    {
        GetComponent<TextMeshPro>().text = "";
        foreach (FarmTask task in tasks)
        {
            if (task.Done)
                GetComponent<TextMeshPro>().text += "<color=\"green\">" + task.Text + "\n" + "</color>";
            else {
                GetComponent<TextMeshPro>().text +=  "<color=\"black\">" + task.Text + "\n" + "</color>" ;
            }
        }
        
    }
}
