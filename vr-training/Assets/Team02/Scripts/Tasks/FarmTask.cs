﻿//============
// Edit: Jan Tošner
//============
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Not used at all.
/// Left for no issues.
/// </summary>
public class FarmTask
{
    private string text;
    private bool done;
    private int number;

    public FarmTask(string text, int number)
    {
        Text = text ?? throw new ArgumentNullException(nameof(text));
        Number = number;
        Done = false;
    }

    public bool Done { get => done; set => done = value; }
    public string Text { get => text; set => text = value; }
    public int Number { get => number; set => number = value; }

    public override string ToString()
    {
        return text;
    }
}
