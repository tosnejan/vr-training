# importing packages
import pandas as pd
import json
import os
from pathlib import Path

path = os.path.dirname(os.getenv('APPDATA')) + "\\LocalLow\\ItWorks studios\\VR-training\\data"
Path("tables").mkdir(parents=True, exist_ok=True)
for name in os.listdir(path):
    for simulation in os.listdir(path + "\\" + name):
        tests = os.listdir(path + "\\" + name + "\\" + simulation)
        df = None
        for test in tests:
            with open(path + "\\" + name + "\\" + simulation + "\\" + test) as f:
                text = f.read()
                data = json.loads(text)
                if df is None:
                    df = pd.DataFrame([data])
                else:
                    df1 = pd.DataFrame([data])
                    df = pd.concat([df, df1])
        df.to_csv(os.path.join("tables", name + "-" + simulation + ".csv"), index=False)
print("Done")
